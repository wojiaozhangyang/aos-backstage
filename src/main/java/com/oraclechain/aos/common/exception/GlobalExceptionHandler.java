package com.oraclechain.aos.common.exception;

import com.oraclechain.aos.common.model.Result;
import com.oraclechain.aos.entity.SysUser;
import com.oraclechain.aos.utils.ServletUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.UnauthenticatedException;
import org.apache.shiro.authz.UnauthorizedException;
import org.apache.shiro.session.UnknownSessionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import java.net.UnknownHostException;

/**
 * @ClassName: GlobalExceptionHandler
 * @Description: Todo
 * @data: 2020/7/14  15:28
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    private Logger logger = LoggerFactory.getLogger(getClass());

    private static int DUPLICATE_KEY_CODE = 1001;
    private static int PARAM_FAIL_CODE = 1002;
    private static int VALIDATION_CODE = 1003;
    private static int AUTH_CODE = 505;


    /**
     * 方法参数校验
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Result handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        logger.error(e.getMessage(), e);
        return Result.fail(PARAM_FAIL_CODE, e.getBindingResult().getFieldError().getDefaultMessage());
    }

    /**
     * 方法参数校验
     */
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public Result HttpMessageNotReadableException(HttpMessageNotReadableException e) {
        logger.error(e.getMessage(), e);
        return Result.fail(PARAM_FAIL_CODE, e.getMessage());
    }


    /**
     * ValidationException
     */
    @ExceptionHandler(ValidationException.class)
    public Result handleValidationException(ValidationException e) {
        logger.error(e.getMessage(), e);
        return Result.fail(VALIDATION_CODE, e.getCause().getMessage());
    }

    /**
     * ConstraintViolationException
     */
    @ExceptionHandler(ConstraintViolationException.class)
    public Result handleConstraintViolationException(ConstraintViolationException e) {
        logger.error(e.getMessage(), e);
        return Result.fail(PARAM_FAIL_CODE, e.getMessage());
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    public Result handlerNoFoundException(Exception e) {
        logger.error(e.getMessage(), e);
        return Result.fail(404, "路径不存在，请检查路径是否正确");
    }

    @ExceptionHandler(DuplicateKeyException.class)
    public Result handleDuplicateKeyException(DuplicateKeyException e) {
        logger.error(e.getMessage(), e);
        return Result.fail(DUPLICATE_KEY_CODE, "数据重复，请检查后提交");
    }


    @ExceptionHandler(Exception.class)
    public Result handleException(Exception e) {
        logger.error(e.getMessage(), e);
        return Result.fail(500, "Business exceptions");
    }

    @ExceptionHandler(MessagingException.class)
    public Result MessagingException(Exception e) {
        logger.error(e.getMessage(), e);
        return Result.fail(500, "Email account error");
    }

    @ExceptionHandler(UnknownHostException.class)
    public Result UnknownHostException(Exception e) {
        logger.error(e.getMessage(), e);
        return Result.fail(500, "Unknown host");
    }


    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public Result HttpRequestMethodNotSupportedException(Exception e) {
        logger.error(e.getMessage(), e);
        return Result.fail(405, "The current request method is not supported");
    }

    @ExceptionHandler(MultipartException.class)
    public Result MultipartException(MultipartException e) {
        logger.error(e.getMessage(), e);
        return Result.fail(405, "File upload exception");

    }

    @ExceptionHandler(UnknownSessionException.class)
    public Result UnknownSessionException(UnknownSessionException e) {
        logger.error(e.getMessage(), e);
        return Result.fail(AUTH_CODE, "Login invalid, please login again");
    }

    /**
     * 权限校验失败 如果请求为ajax返回json，普通请求跳转页面
     */
    @ExceptionHandler(AuthorizationException.class)
    public Object handleAuthorizationException(HttpServletRequest request, AuthorizationException e)
    {
        //开发环境打印异常，正式环境请注销
        if (ServletUtils.isAjaxRequest(request))
        {
            return Result.fail(AUTH_CODE,e.getMessage());
        }
        else
        {
            //shiro异常拦截
            if(e instanceof UnauthorizedException){
                //未授权异常
                return Result.fail(AUTH_CODE,"未授权异常");
            }else if(e instanceof UnauthenticatedException){
                //未认证异常
                return Result.fail(AUTH_CODE,"未认证异常");
            }
            else {
                return Result.fail(AUTH_CODE,"认证异常");

            }
        }
    }


}
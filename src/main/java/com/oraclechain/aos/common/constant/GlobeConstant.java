package com.oraclechain.aos.common.constant;

/**
 * @ClassName: GlobeEnum @Description: Todo
 *
 * @data: 2020/7/21 14:58
 */
public interface GlobeConstant {
  // 天使
  String PARTNER = "1282914726698639361";
  // 群主
  String GROUP = "1282914601016320002";
  // Usdt精度
  Long USDTDECIMAL = Long.valueOf(1000000);
  // Aos精度
  Long AOSDECIMAL = Long.valueOf(10000);
  // EasyApi
  String EasyApiKay = "YS7T2TCQHGH99BQ28UNFRIGMCJI7CRH6TV";

  String IPLANRULEID="1286196731993473026";
  // 系统配置id
  String SYS_WALLET_INFO_ID = "1290628583336514690";
  // usdt合约地址
  String CONTRACT_ADDRESS = "0xdac17f958d2ee523a2206206994597c13d831ec7";

  String ETH_INTERFACE_ADDRESS ="https://api.etherscan.io/api";

  String EosApiKay = "5KjJA5h8rYP4LgGLcdPRvP6pUJduWdcgMpjiAJyap8xAjzZa1N8";

  //邮箱
  String EMAIL_FROM ="aosvictoryteam@gemail.com";
  String EMAIL_USER ="aosvictoryteam";
  String EMAIL_PASS = "a1o2s3v4i5c6t7o8r9y";
  Integer EMAIL_PORT =465;
  String EMAIL_HOST="smtp.gmail.com";

  String CHINESE="Chinese";
  String ENGLISH="English";
  String KOREAN ="Korean";

  String PleaseEnterTheSubscriptionAmount_Chinese="請輸入申購金額";
  String PleaseEnterTheSubscriptionAmount_English="Please enter the subscription amount";
  String PleaseEnterTheSubscriptionAmount_Korean="구입 신청 금액을 입력해 주십시오";

  String TheSubscriptionAmountIsLargerThanAccountAmount_Chinese="申購金額大於賬戶金額";
  String TheSubscriptionAmountIsLargerThanAccountAmount_English="The subscription amount is larger than the account amount";
  String TheSubscriptionAmountIsLargerThanAccountAmount_Korean="구입 신청 금액이 계좌 금액보다 많다";

  String TheMinimumSubscriptionAmountIs_Chinese="申購金額最低爲100";
  String TheMinimumSubscriptionAmountIs_English="The minimum subscription amount is 100";
  String TheMinimumSubscriptionAmountIs_Korean="구입 신청 금액은 최저 100이다.";

  String TheSubscribedAmountIsGreaterThanSubscribedAmount_Chinese="申購金額大於可申購數量";
  String TheSubscribedAmountIsGreaterThanSubscribedAmount_English="The subscribed amount is greater than the subscribed amount";
  String TheSubscribedAmountIsGreaterThanSubscribedAmount_Korean="구입 신청 금액이 구입 가능한 금액보다 많다.";

  String SuccessfulApplications_Chinese="申購成功";
  String SuccessfulApplications_English="Successful applications";
  String SuccessfulApplications_Korean="청약에 성공하다";

  String IrregularAddress_Chinese="地址不符合規則";
  String IrregularAddress_English="Successful applications";
  String IrregularAddress_Korean="주소가 불규칙하다";

  String WithdrawalAmountShouldGreaterHandlingFee_Chinese="提幣數量應該大於手續費";
  String WithdrawalAmountShouldGreaterHandlingFee_English="The withdrawal amount should be greater than the handling fee";
  String WithdrawalAmountShouldGreaterHandlingFee_Korean="화폐인출 수량이 수수료보다 많아야 한다";

  String InvalidVerificationCode_Chinese="無效的驗證碼";
  String InvalidVerificationCode_English="Invalid verification code";
  String InvalidVerificationCode_Korean="잘못된 인증코드";

  String TheInvitationCodeNotExist_Chinese="該邀請碼不存在";
  String TheInvitationCodeNotExist_English="The invitation code does not exist";
  String TheInvitationCodeNotExist_Korean="초대장 번호가 존재하지 않습니다";

  String SendSuccessfully_Chinese="發送成功";
  String SendSuccessfully_English="send successfully";
  String SendSuccessfully_Korean="성공적으로 보내기";

  String ThisNotCurrentlycomplyRules_Chinese="目前不符合升級身份規則";
  String ThisNotCurrentlycomplyRules_English="This does not currently comply with the status escalation rules";
  String ThisNotCurrentlycomplyRules_Korean="현재 신분 업그레이드 규칙에 맞지 않습니다";

  String ExitTheSuccess_Chinese="退出成功";
  String ExitTheSuccess_English="Exit the success";
  String ExitTheSuccess_Korean="성공적으로 끝내기";

  String WrongPassword_Chinese="密碼錯誤";
  String WrongPassword_English="wrong password";
  String WrongPassword_Korean="비밀번호 오류";

  String NonexistentAccount_Chinese="不存在的賬戶";
  String NonexistentAccount_English="Nonexistent account";
  String NonexistentAccount_Korean="존재하지 않는 계정";

  String AbnormalAccount_Chinese="賬戶異常";
  String AbnormalAccount_English="Abnormal account";
  String AbnormalAccount_Korean="계정 이상";


  String LoginFailure_Chinese="登陸失敗";
  String LoginFailure_English="login failure ";
  String LoginFailure_Korean="로그인 실패";

  String PleaseSendTheVerificationCode_Chinese="請先發送驗證碼";
  String PleaseSendTheVerificationCode_English="Please send the verification code first";
  String PleaseSendTheVerificationCode_Korean="먼저 인증샷을 보내주세요";

    String ErrorInMailboxInput_Chinese="邮箱输入错误";
    String ErrorInMailboxInput_English="Error in mailbox input";
    String ErrorInMailboxInput_Korean="편지함 입력 오류";

  String StraightCommission_Chinese="直推返傭";
  String StraightCommission_English="Straight commission";
  String StraightCommission_Korean="줄곧 수수료를 물리다";

  String BeRechargedSuccessfully_Chinese="充值成功";
  String BeRechargedSuccessfully_English="be recharged successfully";
  String BeRechargedSuccessfully_Korean="충전 성공";

  String PurchaseCost_Chinese="申購花費";
  String PurchaseCost_English="Purchase cost";
  String PurchaseCost_Korean="구입 신청 비용";

  String MentionMoneySuccess_Chinese="提幣成功";
  String MentionMoneySuccess_English="Mention money success";
  String MentionMoneySuccess_Korean="화폐 인출에 성공하다.";

  String ReleaseSuccess_Chinese="釋放成功";
  String ReleaseSuccess_English="Release the success";
  String ReleaseSuccess_Korean="석방에 성공하다";

  String CommissionIncome_Chinese="返傭收益";
  String CommissionIncome_English="Release the success";
  String CommissionIncome_Korean="커런트 머니";

}

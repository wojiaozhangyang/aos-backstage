package com.oraclechain.aos.common.wrapper;


/**
 * 国际化属性文件策略
 * <br/>
 */
public interface I18nPropertiesStrategy {

    String getValue(String key);

}
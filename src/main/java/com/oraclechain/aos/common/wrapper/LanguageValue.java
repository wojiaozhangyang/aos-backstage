package com.oraclechain.aos.common.wrapper;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @ClassName: LanguageVal
 * @Description: Todo
 */
@Slf4j
@Component
public class LanguageValue {
    @Autowired
    private Map<String, I18nPropertiesStrategy> i18nPropertiesStrategyMap;

    /**
     * 前端国际化请求头 key
     */
    private static final String LANGUAGE_HEADER = "Language";

    /*
     * 前端请求头 value
     * */
    // 中文
    private static final String CHINESE = "Chinese";
    // 英文
    private static final String ENGLISH = "English";
    // 韩文
    private static final String KOREAN = "korean";
    /**
     * 用于保存请求头中的语言参数
     */
    private String language;


    public String getTipKey( String key,String Language){

        return getI18nVal(key,Language);
    }

    private  String getI18nVal(String langKey,String Language) {
        I18nPropertiesStrategy i18nPropertiesStrategy;
        switch (Language) {
            case CHINESE:
                i18nPropertiesStrategy = i18nPropertiesStrategyMap.get("cnI18nPropertiesStrategy");
                break;
            case ENGLISH:
                i18nPropertiesStrategy = i18nPropertiesStrategyMap.get("enI18nPropertiesStrategy");
                break;
            case KOREAN:
                i18nPropertiesStrategy = i18nPropertiesStrategyMap.get("koI18nPropertiesStrategy");
                break;
            default:
                i18nPropertiesStrategy = i18nPropertiesStrategyMap.get("cnI18nPropertiesStrategy");
                break;
        }
        String value = i18nPropertiesStrategy.getValue(langKey);
        log.info("I18N Filter ### key = {} ---->  value = {}", langKey, value);
        return value;
    }
}

package com.oraclechain.aos.common.wrapper;

import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * 英文国际化属性文件策略
 * <br/>
 *
 * @author ：leigq
 * @date ：2019/7/24 13:04
 */
@Component
 public  class KoI18nPropertiesStrategy implements I18nPropertiesStrategy {

    private volatile static PropertiesUtils propertiesUtils;

    @Override
    public String getValue(String key) {
        return getPropertiesUtils().getValue(key);
    }

    private PropertiesUtils getPropertiesUtils() {
        if (Objects.isNull(propertiesUtils)) {
            synchronized (EnI18nPropertiesStrategy.class) {
                if (Objects.isNull(propertiesUtils)) {
                    propertiesUtils = PropertiesUtils.init("/i18n/language_Kor.properties");
                }
            }
        }
        return propertiesUtils;
    }
}
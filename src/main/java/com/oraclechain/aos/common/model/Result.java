package com.oraclechain.aos.common.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.oraclechain.aos.common.constant.CodeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.Serializable;


@Data
@Component
@Scope("prototype")
@SuppressWarnings(value = "all")
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(value = { "hibernateLazyInitializer", "handler" })
public class Result<T> implements Serializable {

    private T data;
    private Integer code;
    private String msg;

    public static <T> Result<T> ok(String msg) {
        return (Result<T>) of("", CodeEnum.SUCCESS.getCode(), msg);
    }

    public static <T> Result<T> ok(T model, String msg) {
        return of(model, CodeEnum.SUCCESS.getCode(), msg);
    }

    public static <T> Result<T> ok(T model) {
        return of(model, CodeEnum.SUCCESS.getCode(), "");
    }

    public static <T> Result<T> of(T data, Integer code, String msg) {
        return new Result<>(data, code, msg);
    }


    public static <T> Result<T> fail(String msg) {
        return (Result<T>) of("", CodeEnum.ERROR.getCode(), msg);
    }

    public static <T> Result<T> fail(T model, String msg) {
        return of(model, CodeEnum.ERROR.getCode(), msg);
    }

    public static <T> Result<T> fail(Integer code, String msg){
        return (Result<T>) of("", code, msg);
    }

}

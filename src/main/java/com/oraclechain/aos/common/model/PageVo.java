package com.oraclechain.aos.common.model;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @ClassName: PageVo
 * @Description: Todo
 * @data: 2020/7/14  19:12
 */
@Data
public class PageVo {
    @NotNull
    private int pageNum;//页码
    @NotNull
    private int pageSize;//数量
    private String orderByColumn;//排序字段
    private String isAsc;//排序字符 asc desc

}

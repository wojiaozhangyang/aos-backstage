package com.oraclechain.aos.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.oraclechain.aos.common.model.PageVo;
import com.oraclechain.aos.common.model.Result;
import com.oraclechain.aos.entity.RebateRecord;
import com.oraclechain.aos.entity.ShareRecord;
import com.oraclechain.aos.entity.SysPermission;
import com.oraclechain.aos.entity.SysUser;
import com.oraclechain.aos.service.IShareRecordService;
import com.oraclechain.aos.service.ISysUserService;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.MapUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 * 分享记录 前端控制器
 * </p>
 *
 * @since 2020-07-17
 */
@RestController
@RequestMapping("/ShareRecord")
public class ShareRecordController {

    @Autowired
    private IShareRecordService iShareRecordService;

    @Autowired
    private ISysUserService iSysUserService;

    @ApiOperation(value = "保存或更新", notes = "保存或更新")
    @PostMapping("/saveOrUpdate")
    //@RequiresPermissions("system:shareRecord:saveOrUpdate")
    @ResponseBody
    public Result saveOrUpdate(@RequestBody ShareRecord shareRecord){
        // 获取当前用户信息
        SysUser principal = (SysUser) SecurityUtils.getSubject().getPrincipal();
        shareRecord.setUserId(principal.getId());
        shareRecord.setStatus(0);
        shareRecord.setUserEmail(principal.getEmail());
        SysUser sysUser = new SysUser();
        sysUser.setId(principal.getId());
        sysUser.setAuditStatus(2);
        iSysUserService.updateById(sysUser);
        return  Result.ok(iShareRecordService.saveOrUpdate(shareRecord));
    }


    @ApiOperation(value = "审批", notes = "审批")
    @PostMapping("/audit")
    @ResponseBody
    public Result saveOrUpdate(@RequestBody Map<String, Object> params){
        String id = MapUtils.getString(params, "id");
        ShareRecord shareRecord = iShareRecordService.getById(id);
        shareRecord.setStatus(1);
        SysUser sysUser = iSysUserService.getOne(new QueryWrapper<SysUser>().eq("id", shareRecord.getUserId()));
        sysUser.setAuditStatus(1);
        iSysUserService.updateById(sysUser);
        return  Result.ok(iShareRecordService.saveOrUpdate(shareRecord));
    }



    @ApiOperation(value = "查询列表", notes = "查询列表")
    @PostMapping("/list")
    @ResponseBody
    public Result list(){
        return  Result.ok( iShareRecordService.list());
    }


    @ApiOperation(value = "分页查询", notes = "分页查询")
    @PostMapping("/page")
    @ResponseBody
    public Result page(@RequestBody PageVo pageVo){
        Page<ShareRecord> page = new Page<>(pageVo.getPageNum(),pageVo.getPageSize());
        IPage<ShareRecord> shareRecordIPage = iShareRecordService.page(page,new QueryWrapper<ShareRecord>().eq("status",0));
        return  Result.ok(shareRecordIPage);
    }
}

package com.oraclechain.aos.controller;

import com.oraclechain.aos.common.model.Result;
import com.oraclechain.aos.service.IUsdtEthRecordService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 前端控制器
 *
 * @since 2020-07-19
 */
@EnableAsync
@RestController
@EnableScheduling
@RequestMapping("/UsdtEthRecord")
public class UsdtEthRecordController {
  @Autowired private IUsdtEthRecordService iUsdtEthRecordService;


  @Scheduled(fixedRate = 15000)
  @Async
  public void SelectUsdtRecord() {
    iUsdtEthRecordService.SelectUsdtRecord();
  }




    //只执行一次

    @Scheduled(initialDelay = 1000, fixedRate = Long.MAX_VALUE)
    public void addUsdtRecord() {
        iUsdtEthRecordService.addUsdtRecord();
    }



  @ApiOperation(value = "获取昨日收盘价", notes = "获取昨日收盘价")
  @PostMapping("/getUsdtToAosRecords")
  @ResponseBody
  public Result getUsdtToAosRecords() {
    List list = iUsdtEthRecordService.getUsdtToAosRecords();
    // 收盘价
    return Result.ok(list.get(4));
  }




}

package com.oraclechain.aos.controller;

import cn.hutool.extra.mail.MailAccount;
import cn.hutool.extra.mail.MailUtil;
import cn.hutool.extra.qrcode.QrCodeUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.oraclechain.aos.common.constant.GlobeConstant;
import com.oraclechain.aos.common.model.PageVo;
import com.oraclechain.aos.common.model.Result;
import com.oraclechain.aos.entity.*;
import com.oraclechain.aos.entity.export.UserExport;
import com.oraclechain.aos.service.*;
import com.oraclechain.aos.utils.ExcelUtil;
import com.oraclechain.aos.utils.FastDFSClientUtil;
import com.oraclechain.aos.utils.RandomStringUtil;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户表 前端控制器
 *
 * @since 2020-07-14
 */
@Slf4j
@EnableScheduling
@RestController
@RequestMapping("/user")
public class SysUserController {

  Integer number = 0;

  @Autowired private ISysUserService iSysUserService;

  @Autowired private ISysRoleUserService iSysRoleUserService;

  @Autowired private IUserAccountService iUserAccountService;
  @Autowired private ISysRoleService iSysRoleService;

  @Autowired  private IUsdtAddtressService iUsdtAddtressService;

  @PostMapping("register")
  @ApiOperation(value = "正常注册(邮箱注册)", notes = "正常注册(邮箱注册)")
  @ResponseBody
  public Result loginUserRegister(@RequestBody @Validated SysUser user,@RequestHeader("Language") String language) throws Exception {
    SysUser sysUser =
        iSysUserService.getOne(new QueryWrapper<SysUser>().eq("email", user.getEmail()));
    if (sysUser != null) {
      if (user.getEmailCode()!=null && !sysUser.getEmailCode().equals(user.getEmailCode()))
       {
         if (language.equals(GlobeConstant.CHINESE)){ return Result.fail(GlobeConstant.InvalidVerificationCode_Chinese);}
         if (language.equals(GlobeConstant.ENGLISH)){ return Result.fail(GlobeConstant.InvalidVerificationCode_English);}
         if (language.equals(GlobeConstant.KOREAN)){ return Result.fail(GlobeConstant.InvalidVerificationCode_Korean);}
       }
      // 设置邀请人
      if (StringUtils.isNotBlank(user.getInviteCode())) {
        SysUser inviteUser =
            iSysUserService.getOne(
                new QueryWrapper<SysUser>().eq("invite_code", user.getInviteCode()));
        if (inviteUser == null) {
          if (language.equals(GlobeConstant.CHINESE)){ return Result.fail(GlobeConstant.TheInvitationCodeNotExist_Chinese);}
          if (language.equals(GlobeConstant.ENGLISH)){ return Result.fail(GlobeConstant.TheInvitationCodeNotExist_English);}
          if (language.equals(GlobeConstant.KOREAN)){ return Result.fail(GlobeConstant.TheInvitationCodeNotExist_Korean);}
        }
        sysUser.setInviterBy(inviteUser.getId());
      }

      // 生成盐
      sysUser.setSalt(new SecureRandomNumberGenerator().nextBytes().toHex());
      // 加密密码
      String newPassword = passwordHandle(user.getPassword(), sysUser.getSalt());
      sysUser.setSalt(sysUser.getSalt());
      sysUser.setPassword(newPassword);
      // 邀请码
      String randString = RandomStringUtil.getRandString(6);
      Boolean flag = true;
      while (flag) {
        SysUser inviteUser =
            iSysUserService.getOne(new QueryWrapper<SysUser>().eq("invite_code", randString));
        if (inviteUser != null) {
          randString = RandomStringUtil.getRandString(6);
        } else {
          sysUser.setInviteCode(randString);
        }
        flag = false;
      }
      sysUser.setEmailStatus(1);
      // 生成钱包地址
      List<UsdtAddtress> usdtAddtresses = iUsdtAddtressService.list(new QueryWrapper<UsdtAddtress>().eq("is_use", 0));
      sysUser.setWalletAddress(usdtAddtresses.get(1).getAddress());
      usdtAddtresses.get(1).setIsUse(1);
      iUsdtAddtressService.updateById(usdtAddtresses.get(1));
      BufferedImage image = QrCodeUtil.generate(sysUser.getWalletAddress(), 300, 300);
      String path = FastDFSClientUtil.saveFile(getImageStream(image), user.getEmail(), "png");

      sysUser.setQrCodePath(path);
      iSysUserService.updateById(sysUser);

      // 添加角色
      SysRoleUser sysRoleUser = new SysRoleUser();
      sysRoleUser.setSysRoleId(GlobeConstant.PARTNER);
      sysRoleUser.setSysUserId(sysUser.getId());
      boolean save = iSysRoleUserService.save(sysRoleUser);

      UserAccount userAccount = new UserAccount();
      userAccount.setUsdtBalance(Long.valueOf(0));
      userAccount.setAosBalance(Long.valueOf(0));
      userAccount.setUserId(sysUser.getId());
      iUserAccountService.save(userAccount);

      return Result.ok(save);
    } else {
      if (language.equals(GlobeConstant.CHINESE)){ return Result.fail(GlobeConstant.PleaseSendTheVerificationCode_Chinese);}
      if (language.equals(GlobeConstant.ENGLISH)){ return Result.fail(GlobeConstant.PleaseSendTheVerificationCode_English);}
      if (language.equals(GlobeConstant.KOREAN)){ return Result.fail(GlobeConstant.PleaseSendTheVerificationCode_Korean);}
      return null;
    }
  }

  @GetMapping("sendEmail")
  @ApiOperation(value = "发送验证码", notes = "邮箱激活")
  public Result sendEmail(@RequestHeader("Language") String language ,@RequestParam("email") String email) {

    String emailCode = com.oraclechain.aos.utils.StringUtils.generateRandomStr(4);
    SysUser sysUser = iSysUserService.getOne(new QueryWrapper<SysUser>().eq("email", email));
    MailAccount account = new MailAccount();
    account.setHost(GlobeConstant.EMAIL_HOST);
    account.setPort(GlobeConstant.EMAIL_PORT);
    account.setAuth(true);
    account.setSslEnable(true);
    account.setFrom(GlobeConstant.EMAIL_FROM);
    account.setUser(GlobeConstant.EMAIL_USER);
    account.setPass(GlobeConstant.EMAIL_PASS);
    if (sysUser != null) {
      String EmailCodeStr = "您的激活码为：" + sysUser.getEmailCode();
      MailUtil.send(account, email, "AOS activate", EmailCodeStr, false);
    } else {
      String EmailCodeStr = "您的激活码为：" + emailCode;
      SysUser user = new SysUser();
      user.setEmail(email);
      user.setEmailCode(emailCode);
      user.setEmailStatus(0);
      user.setAuditStatus(0);
      iSysUserService.save(user);
      MailUtil.send(account, email, "AOS Registration activation", EmailCodeStr, false);
    }
    if (language.equals(GlobeConstant.CHINESE)){ return Result.ok(GlobeConstant.SendSuccessfully_Chinese);}
    if (language.equals(GlobeConstant.ENGLISH)){ return Result.ok(GlobeConstant.SendSuccessfully_English);}
    if (language.equals(GlobeConstant.KOREAN)){ return Result.ok(GlobeConstant.SendSuccessfully_Korean);}
    return Result.ok("");
  }

  /**
   * 用户登陆验证
   *
   * @param user
   * @return
   */
  @ApiOperation(value = "用户登陆验证", notes = "用户登陆验证")
  @PostMapping("/login")
  @ResponseBody
  public Result login(@RequestBody SysUser user) {
    AuthenticationToken token;
    token = new UsernamePasswordToken(user.getEmail(), user.getPassword(),true);
    // 获取shirio Subject
    Subject subject = SecurityUtils.getSubject();
    try {
      subject.login(token);
    } catch (IncorrectCredentialsException e) {
      return Result.fail("密码错误");
    } catch (UnknownAccountException e) {
      return Result.fail("不存在的账户");
    } catch (LockedAccountException e) {
      return Result.fail("账户异常");
    } catch (Exception e) {
      return Result.fail("登陆失败：" + e.getMessage());
    }
    SysUser userInfo = (SysUser) subject.getPrincipal();
    subject.getSession().setAttribute("userInfo", userInfo);

    Serializable id = subject.getSession().getId();
    SysRoleUser sysRoleUser =
        iSysRoleUserService.getOne(
            new QueryWrapper<SysRoleUser>().eq("sys_user_id", userInfo.getId()));

    SysRole role =
        iSysRoleService.getOne(new QueryWrapper<SysRole>().eq("id", sysRoleUser.getSysRoleId()));
    Map<String, Object> result = new HashMap<>();
    result.put("sessionId", id);
    result.put("user", userInfo);
    result.put("role", role);
    return Result.ok(result);
  }



  @ApiOperation(value = "密码修改", notes = "密码修改")
  @PostMapping("/changePassword")
  @ResponseBody
  public Result changePassword(@RequestBody SysUser user,@RequestHeader("Language") String language) {
    SysUser sysUser =
            iSysUserService.getOne(new QueryWrapper<SysUser>().eq("email", user.getEmail()));
    if (sysUser != null) {
      if (user.getEmailCode()!=null && !sysUser.getEmailCode().equals(user.getEmailCode()))
      {
        if (language.equals(GlobeConstant.CHINESE)){ return Result.fail(GlobeConstant.InvalidVerificationCode_Chinese);}
        if (language.equals(GlobeConstant.ENGLISH)){ return Result.fail(GlobeConstant.InvalidVerificationCode_English);}
        if (language.equals(GlobeConstant.KOREAN)){ return Result.fail(GlobeConstant.InvalidVerificationCode_Korean);}
      }
      String newPassword = passwordHandle(user.getPassword(), sysUser.getSalt());
      sysUser.setPassword(newPassword);
      return Result.ok(iSysUserService.updateById(sysUser));
    } else {
      if (language.equals(GlobeConstant.CHINESE)){ return Result.fail(GlobeConstant.ErrorInMailboxInput_Chinese);}
      if (language.equals(GlobeConstant.ENGLISH)){ return Result.fail(GlobeConstant.ErrorInMailboxInput_English);}
      if (language.equals(GlobeConstant.KOREAN)){ return Result.fail(GlobeConstant.ErrorInMailboxInput_Korean);}
      return null;
    }

  }



  @ApiOperation(value = "获取二维码", notes = "获取二维码")
  @GetMapping("/getQrCode")
  public void createQrCodeN(HttpServletResponse response) throws IOException {
    SysUser principal = (SysUser) SecurityUtils.getSubject().getPrincipal();
    BufferedImage image = QrCodeUtil.generate(principal.getWalletAddress(), 300, 300);


    OutputStream out = response.getOutputStream();
    ImageIO.write(image, "jpg", out);
    out.close();
  }

  /**
   * 退出登陆
   *
   * @return
   */
  @ApiOperation(value = "退出登陆", notes = "退出登陆")
  @GetMapping("/Loginout")
  public Result LoginOut(@RequestHeader("Language") String language ,HttpServletRequest request, HttpServletResponse response) {
    // 在这里执行退出系统前需要清空的数据
    Subject subject = SecurityUtils.getSubject();
    // 注销
    subject.logout();
    if (language.equals(GlobeConstant.CHINESE)){ return Result.ok(GlobeConstant.ExitTheSuccess_Chinese);}
    if (language.equals(GlobeConstant.ENGLISH)){ return Result.ok(GlobeConstant.ExitTheSuccess_English);}
    if (language.equals(GlobeConstant.KOREAN)){ return Result.ok(GlobeConstant.ExitTheSuccess_Korean);}

    return Result.ok("退出成功");
  }

  @ApiOperation(value = "我的邀请", notes = "我的邀请人数")
  @PostMapping("/invitePeople")
  @ResponseBody
  public Result Invite() {
    // 获取当前用户信息
    SysUser principal = (SysUser) SecurityUtils.getSubject().getPrincipal();
    return Result.ok(myInvite(principal));
  }

  @ApiOperation(value = "检查角色", notes = "检查角色")
  @Scheduled(fixedRate = 500000)
  public void checkLevel() {
    // 获取当前用户信息
    List<SysUser> sysUsers = iSysUserService.list(new QueryWrapper<SysUser>().eq("is_upgrade", 0));
    for (int j = 0; j < sysUsers.size(); j++) {
      // 获取直推好友
      List<SysUser> inviterByList =
          iSysUserService.list(
              new QueryWrapper<SysUser>()
                  .eq("inviter_by", sysUsers.get(j).getId())
                  .eq("email_status", 1));

      List inviterByIds = new ArrayList();
      List<SysUser> indirectByList = null;
      // 获取间推好友
      if (inviterByList != null && inviterByList.size() > 0) {
        inviterByList.stream()
            .forEach(
                i -> {
                  inviterByIds.add(i.getId());
                });
        indirectByList =
            iSysUserService.list(
                new QueryWrapper<SysUser>().in("inviter_by", inviterByIds).eq("email_status", 1));
      }
      Map<String, Object> result = new HashMap<>();
      // 直接邀请
      result.put("directList", inviterByList.size() > 0 ? inviterByList : 0);
      Integer indirectConut = indirectByList != null ? indirectByList.size() : 0;
      result.put("indirectCount", indirectByList != null ? indirectByList : 0);

      if (inviterByList.size() + indirectConut > 50) {
        // -----升级身份
        SysRoleUser roleUser =
            iSysRoleUserService.getOne(
                new QueryWrapper<SysRoleUser>().eq("sys_user_id", sysUsers.get(j).getId()));
        roleUser.setSysRoleId("1282914601016320002");
        iSysRoleUserService.updateById(roleUser);

      } else {
        // ----判断第二种
        if (inviterByList != null && inviterByList.size() > 5) {
          inviterByList.stream()
              .forEach(
                  i -> {
                    List<SysUser> list =
                        iSysUserService.list(
                            new QueryWrapper<SysUser>()
                                .eq("inviter_by", i.getId())
                                .eq("email_status", 1));
                    if (list != null && list.size() > 5) {
                      number++;
                    }
                  });
        }
        if (number > 5) {
          // 升级身份
          sysUsers.get(j).setIsUpgrade(1);
          iSysUserService.updateById(sysUsers.get(j));
        }
      }
    }
  }

  @ApiOperation(value = "升级角色", notes = "升级角色")
  @PostMapping("/updateLevel")
  @ResponseBody
  public Result updateLevel(@RequestHeader("Language") String language ) {
    // 获取当前用户信息
    SysUser principal = (SysUser) SecurityUtils.getSubject().getPrincipal();

    Map<String, Object> map = myInvite(principal);
    List<SysUser> inviterByList = (List<SysUser>) map.get("directList");
    List<SysUser> indirectCount = (List<SysUser>) map.get("indirectCount");
    if (inviterByList.size() + indirectCount.size() > 50) {
      // -----升级身份
      SysRoleUser roleUser =
          iSysRoleUserService.getOne(
              new QueryWrapper<SysRoleUser>().eq("sys_user_id", principal.getId()));
      roleUser.setSysRoleId("1282914601016320002");
      iSysRoleUserService.updateById(roleUser);

    } else {
      // ----判断第二种
      if (inviterByList != null && inviterByList.size() > 5) {
        inviterByList.stream()
            .forEach(
                i -> {
                  List<SysUser> list =
                      iSysUserService.list(
                          new QueryWrapper<SysUser>()
                              .eq("inviter_by", i.getId())
                              .eq("email_status", 1));
                  if (list != null && list.size() > 5) {
                    number++;
                  }
                });
      }
      if (number > 5) {
        // 升级身份
        SysRoleUser roleUser =
            iSysRoleUserService.getOne(
                new QueryWrapper<SysRoleUser>().eq("sys_user_id", principal.getId()));
        roleUser.setSysRoleId(GlobeConstant.GROUP);
        iSysRoleUserService.updateById(roleUser);
      } else {
        if (language.equals(GlobeConstant.CHINESE)){ return Result.fail(GlobeConstant.ThisNotCurrentlycomplyRules_Chinese);}
        if (language.equals(GlobeConstant.ENGLISH)){ return Result.fail(GlobeConstant.ThisNotCurrentlycomplyRules_English);}
        if (language.equals(GlobeConstant.KOREAN)){ return Result.fail(GlobeConstant.ThisNotCurrentlycomplyRules_Korean);}
      }
    }

    return Result.ok("");
  }

  @ApiOperation(value = "导出邀请人列表", notes = "导出邀请人列表")
  @GetMapping("/export")
  @ResponseBody
  public void export(HttpServletResponse response) throws IOException {

    List<SysUser> allUserList = iSysUserService.list();
    List<UserExport> userExports = new ArrayList<>();
    for (int u = 0; u < allUserList.size(); u++) {
      // 获取直推好友
      List<SysUser> inviterByList =
          iSysUserService.list(
              new QueryWrapper<SysUser>()
                  .eq("inviter_by", allUserList.get(u).getId())
                  .eq("email_status", 1));

      List inviterByIds = new ArrayList();
      List<SysUser> indirectByList = null;
      // 获取间推好友
      if (inviterByList != null && inviterByList.size() > 0) {
        inviterByList.stream()
            .forEach(
                i -> {
                  inviterByIds.add(i.getId());
                });
        indirectByList =
            iSysUserService.list(
                new QueryWrapper<SysUser>().in("inviter_by", inviterByIds).eq("email_status", 1));
      }

      if (inviterByList.size() > 0) {
        for (int i = 0; i < inviterByList.size(); i++) {
          UserExport userExport = new UserExport();
          userExport.setEmail(allUserList.get(i).getEmail());
          userExport.setInviterBy(inviterByList.get(i).getEmail());
          userExport.setPushInfo("直推好友");
          userExports.add(userExport);
        }
      }
      if (indirectByList!=null && indirectByList.size() > 0) {
        for (int i = 0; i < indirectByList.size(); i++) {
          UserExport userExport = new UserExport();
          userExport.setInviterBy(allUserList.get(i).getEmail());
          userExport.setEmail(indirectByList.get(i).getEmail());
          userExport.setPushInfo("间推好友");
          userExports.add(userExport);
        }
      }
    }

    ExcelUtil.exportExcel(userExports, "邀请记录", "邀请用户信息", UserExport.class, "User", response);
  }

  @ApiOperation(value = "邀请人列表", notes = "邀请人列表")
  @GetMapping("/list")
  @ResponseBody
  public Result list()  {
    List<SysUser> allUserList = iSysUserService.list();
    return Result.ok(allUserList);
  }

  @ApiOperation(value = "邀请人分頁", notes = "邀请人分頁")
  @PostMapping("/page")
  @ResponseBody
  public Result page(@RequestBody PageVo pageVo)  {
    Page<SysUser> page1 = new Page<>(pageVo.getPageNum(), pageVo.getPageSize());
    Page<SysUser> allUserList = iSysUserService.page(page1);
    return Result.ok(allUserList);
  }


  public Map<String, Object> myInvite(SysUser principal) {
    // 获取直推好友
    List<SysUser> inviterByList =
            iSysUserService.list(
                    new QueryWrapper<SysUser>()
                            .eq("inviter_by", principal.getId())
                            .eq("email_status", 1));

    List inviterByIds = new ArrayList();
    List<SysUser> indirectByList = null;
    // 获取间推好友
    if (inviterByList != null && inviterByList.size() > 0) {
      inviterByList.stream()
              .forEach(
                      i -> {
                        inviterByIds.add(i.getId());
                      });
      indirectByList =
              iSysUserService.list(
                      new QueryWrapper<SysUser>().in("inviter_by", inviterByIds).eq("email_status", 1));
    }
    Map<String, Object> result = new HashMap<>();
    // 直接邀请
    result.put("directList", inviterByList.size() > 0 ? inviterByList : 0);
    result.put("indirectCount", indirectByList != null ? indirectByList : 0);
    return result;
  }


  /**
   * 密码处理
   *
   * @param password
   * @param credentialsSalt
   * @return
   */
  private String passwordHandle(String password, String credentialsSalt) {
    return new SimpleHash(
            "md5",
            password, // 原密码
            ByteSource.Util.bytes(credentialsSalt),
            2)
        .toHex();
  }

  public InputStream getImageStream(BufferedImage bimage){
    InputStream is = null;
    ByteArrayOutputStream bs = new ByteArrayOutputStream();
    ImageOutputStream imOut;
    try {
      imOut = ImageIO.createImageOutputStream(bs);
      ImageIO.write(bimage, "png",imOut);
      is= new ByteArrayInputStream(bs.toByteArray());
    } catch (IOException e) {
      e.printStackTrace();
    }
    return is;
  }
}

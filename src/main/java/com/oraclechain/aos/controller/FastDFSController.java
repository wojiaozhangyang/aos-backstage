package com.oraclechain.aos.controller;
import cn.hutool.core.util.CharUtil;
import com.oraclechain.aos.common.model.Result;
import com.oraclechain.aos.entity.FastDFSFile;
import com.oraclechain.aos.entity.FileInfo;
import com.oraclechain.aos.service.IFileInfoService;
import com.oraclechain.aos.utils.FastDFSClientUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
/**
 * @ClassName: FastDFSController
 * @Description: Todo
 * @data: 2020/7/17  12:11
 */


@RestController
@RequestMapping("/fastdfs")
public class FastDFSController {
    private static Logger logger = LoggerFactory.getLogger(FastDFSController.class);

    @Autowired
    private IFileInfoService iFileInfoService;
    /**
     * 文件上传
     * @param file 文件
     * @return
     */
    @RequestMapping("/upload")
    @ResponseBody
    public Result singleFileUpload(@RequestParam("file") MultipartFile file) {
        HashMap<Object, Object> map = new HashMap<>(16);

        if (file.isEmpty()) {
            return Result.fail("请选择要上传的文件！");
        }
        try {
            String path = saveFile(file);
            FileInfo fileInfo = new FileInfo();
            // 将文件的md5设置为文件表的id
            fileInfo.setName(file.getOriginalFilename());
            fileInfo.setContentType(file.getContentType());
            fileInfo.setSize(file.getSize());
            fileInfo.setCreateTime(new Date());
            fileInfo.setUrl(path);
            iFileInfoService.save(fileInfo);
            map.put("fileInfo",iFileInfoService.getById(fileInfo));
        } catch (Exception e) {
            logger.error("上传失败！", e);
           return  Result.fail("上传失败！" + e.getMessage());
        }
        return  Result.ok(map);
    }

    /**
     * @param multipartFile 文件
     * @return
     * @throws IOException
     */
    public String saveFile(MultipartFile multipartFile) throws IOException {
        String[] fileAbsolutePath={};
        byte[] file_buff = null;

        String fileName = multipartFile.getOriginalFilename();
        String ext = fileName.substring(fileName.lastIndexOf(".") + 1);
        InputStream inputStream = multipartFile.getInputStream();

        if(inputStream != null){
            int len1 = inputStream.available();
            file_buff = new byte[len1];
            inputStream.read(file_buff);
        }

        inputStream.close();

        FastDFSFile file = new FastDFSFile(fileName, file_buff, ext);

        try {
            fileAbsolutePath = FastDFSClientUtil.upload(file);
        } catch (Exception e) {
            logger.error("文件上传异常!", e);
        }

        if (fileAbsolutePath==null) {
            logger.error("上传文件失败，请再次上传!");
        }

        StringBuffer buffer = new StringBuffer();
        buffer.append(FastDFSClientUtil.getTrackerUrl());
        buffer.append(fileAbsolutePath[0]);
        buffer.append(CharUtil.SLASH);
        buffer.append(fileAbsolutePath[1]);

        return buffer.toString();
    }
}
package com.oraclechain.aos.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.oraclechain.aos.common.model.Result;
import com.oraclechain.aos.entity.PlanRule;
import com.oraclechain.aos.entity.SysUser;
import com.oraclechain.aos.entity.UserAccount;
import com.oraclechain.aos.service.IUserAccountService;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户账户 前端控制器
 * </p>
 *
 * @since 2020-07-16
 */
@RestController
@RequestMapping("/UserAccount")
public class UserAccountController {

    @Autowired
    private IUserAccountService iUserAccountService;

    @ApiOperation(value = "保存或更新", notes = "保存或更新")
    @PostMapping("/saveOrUpdate")
    public Result saveOrUpdate(@RequestBody UserAccount userAccount) {
        return Result.ok(iUserAccountService.saveOrUpdate(userAccount));
    }

    @ApiOperation(value = "获取当前用户账户", notes = "获取当前用户账户")
    @PostMapping("/currentUserAccount")
    public Result currentUserAccount() {
        SysUser principal = (SysUser) SecurityUtils.getSubject().getPrincipal();
        return Result.ok(iUserAccountService.getOne(new QueryWrapper<UserAccount>().eq("user_id",principal.getId())));
    }



}

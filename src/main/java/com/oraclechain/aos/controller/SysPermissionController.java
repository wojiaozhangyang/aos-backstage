package com.oraclechain.aos.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.oraclechain.aos.common.model.PageVo;
import com.oraclechain.aos.common.model.Result;
import com.oraclechain.aos.entity.SysPermission;
import com.oraclechain.aos.service.ISysPermissionService;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 权限表 前端控制器
 * </p>
 *
 * @since 2020-07-14
 */
@RestController
@RequestMapping("/SysPermission")
public class SysPermissionController {

    @Autowired
    private ISysPermissionService iSysPermissionService;

    /**
     * 权限列表
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @PostMapping("/page")
    @RequiresPermissions("system:permission:page")
    @ResponseBody
    public Result page(@RequestBody PageVo pageVo){
        Page<SysPermission> page = new Page<>(pageVo.getPageNum(),pageVo.getPageSize());
        IPage<SysPermission> sysPermissions = iSysPermissionService.page(page);
        return  Result.ok(sysPermissions);
    }




    /**
     * 权限列表
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @PostMapping("/list")
    @RequiresPermissions("system:permission:list")
    @ResponseBody
    public Result list(){
        List<SysPermission> list = iSysPermissionService.list();
        return  Result.ok(list);
    }
    /**
     * 权限添加
     * @param role
     * @return
     */
    //@Log(title = "权限添加", action = "1")
    @ApiOperation(value = "新增", notes = "新增")
    @PostMapping("/add")
    @RequiresPermissions("system:permission:add")
    @ResponseBody
    public Result add(@RequestBody SysPermission role){
        boolean b= iSysPermissionService.save(role);
       return  Result.ok("");
    }

    /**
     * 删除权限
     * @param ids
     * @return
     */
    @ApiOperation(value = "删除", notes = "删除")
    @PostMapping("/remove")
    @RequiresPermissions("system:permission:remove")
    @ResponseBody
    public Result remove(String ids){
        int b= iSysPermissionService.deleteByPrimaryKey(ids);
//        if(b==1){
//            return success();
//        }else if(b==-1){
//            return error("该权限有子权限，请先删除子权限");
//        }else if(b==-2){
//            return error("该权限绑定了角色，请解除角色绑定");
//        }else {
//            return error();
//        }
        return null;
    }

//    /**
//     * 检查权限
//     * @param SysPermission
//     * @return
//     */
//    @ApiOperation(value = "检查权限", notes = "检查权限")
//    @PostMapping("/checkNameUnique")
//    @ResponseBody
//    public int checkNameUnique(SysPermission SysPermission){
//        int b= iSysPermissionService.checkNameUnique(SysPermission);
//        if(b>0){
//            return 1;
//        }else{
//            return 0;
//        }
//    }
//
//    /**
//     * 检查权限URL
//     * @param tsysUser
//     * @return
//     */
//    @ApiOperation(value = "检查权限URL", notes = "检查权限URL")
//    @PostMapping("/checkURLUnique")
//    @ResponseBody
//    public int checkURLUnique(SysPermission tsysPermission){
//        int b= sysPermissionService.checkURLUnique(tsysPermission);
//        if(b>0){
//            return 1;
//        }else{
//            return 0;
//        }
//    }
//
//    /**
//     * 检查权限perms字段
//     * @param tsysUser
//     * @return
//     */
//    @ApiOperation(value = "检查权限perms字段", notes = "检查权限perms字段")
//    @PostMapping("/checkPermsUnique")
//    @ResponseBody
//    public int checkPermsUnique(SysPermission tsysPermission){
//        int b= iSysPermissionService.checkPermsUnique(tsysPermission);
//        if(b>0){
//            return 1;
//        }else{
//            return 0;
//        }
//    }
//
//
//    /**
//     * 修改保存权限
//     */
//    //@Log(title = "修改保存权限", action = "1")
//    @ApiOperation(value = "修改保存", notes = "修改保存")
//    @RequiresPermissions("system:permission:edit")
//    @PostMapping("/edit")
//    @ResponseBody
//    public Result editSave(SysPermission SysPermission){
//        return Result.ok(iSysPermissionService.updateById(SysPermission));
//    }
//
//
//
//    /**
//     * 根据角色id获取bootstarp 所有打勾权限
//     * @param roleId 角色id集合
//     * @return
//     */
//    @ApiOperation(value = "根据角色id获取bootstarp 所有打勾权限", notes = "根据角色id获取bootstarp 所有打勾权限")
//    @PostMapping("/getCheckPrem")
//    @ResponseBody
//    public Result getCheckPrem(String roleId){
//        iSysPermissionService.getCheckPrem(roleId);
//        return Result.ok();
//    }





}

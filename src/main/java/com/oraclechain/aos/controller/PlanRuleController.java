package com.oraclechain.aos.controller;


import com.oraclechain.aos.common.constant.GlobeConstant;
import com.oraclechain.aos.common.model.Result;
import com.oraclechain.aos.entity.PlanRule;
import com.oraclechain.aos.service.IPlanRuleService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @since 2020-07-23
 */
@RestController
@RequestMapping("/planRule")
public class PlanRuleController {

    @Autowired
    private IPlanRuleService iPlanRuleService;


    @ApiOperation(value = "保存或更新", notes = "保存或更新")
    @PostMapping("/saveOrUpdate")
    public Result saveOrUpdate(@RequestBody PlanRule planRule) {
        return Result.ok(iPlanRuleService.saveOrUpdate(planRule));
    }


    @ApiOperation(value = "手续费", notes = "手续费")
    @PostMapping("/fee")
    public Result fee() {
        return Result.ok(1* GlobeConstant.USDTDECIMAL);
    }

}

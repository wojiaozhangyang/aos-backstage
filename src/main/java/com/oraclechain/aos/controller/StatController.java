package com.oraclechain.aos.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.api.R;
import com.oraclechain.aos.common.model.Result;
import com.oraclechain.aos.entity.PurchaseRecords;
import com.oraclechain.aos.entity.RebateRecord;
import com.oraclechain.aos.service.IPurchaseRecordsService;
import com.oraclechain.aos.service.IRebateRecordService;
import com.oraclechain.aos.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: StatController @Description: Todo
 *
 * @data: 2020/8/3 19:55
 */
@RestController
@RequestMapping("/stat")
public class StatController {

  @Autowired private ISysUserService iSysUserService;
  @Autowired private IPurchaseRecordsService iPurchaseRecordsService;
  @Autowired private IRebateRecordService iRebateRecordService;

  @PostMapping("index")
  public Result stat() {

    // 申购总金额
    List<PurchaseRecords> purchaseRecordsList = iPurchaseRecordsService.list();
    Long purchaseCount = Long.valueOf(0);
    for (int i = 0; i < purchaseRecordsList.size(); i++) {
      purchaseCount =
          new BigDecimal(purchaseRecordsList.get(i).getUsdtAmountCosts())
              .add(new BigDecimal(purchaseCount))
              .longValue();
    }
    // 返佣总金额
    List<RebateRecord> rebateList =
        iRebateRecordService.list(
            new QueryWrapper<RebateRecord>().eq("type", 1).eq("status", 1).eq("is_release", 3));
      Long rebateCount = Long.valueOf(0);
      for (int i = 0; i < rebateList.size(); i++) {
          rebateCount =
                  new BigDecimal(rebateList.get(i).getAmount())
                          .add(new BigDecimal(rebateCount))
                          .longValue();
      }
    //释放总金额
    List<RebateRecord> releaseList =
        iRebateRecordService.list(
            new QueryWrapper<RebateRecord>().eq("type", 1).eq("status", 1).ne("is_release", 3));
      Long releaseCount = Long.valueOf(0);
      for (int i = 0; i < releaseList.size(); i++) {
          releaseCount =
                  new BigDecimal(releaseList.get(i).getAmount())
                          .add(new BigDecimal(releaseCount))
                          .longValue();
      }


    Map map = new HashMap();
    map.put("PeopleCount", iSysUserService.list().size());
    map.put("PurchaseCount", purchaseCount);
    map.put("rebateCount", rebateCount);
    map.put("releaseCount", releaseCount);
    return Result.ok(map);
  }
}

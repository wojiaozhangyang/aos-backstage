package com.oraclechain.aos.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.oraclechain.aos.common.model.PageVo;
import com.oraclechain.aos.common.model.Result;
import com.oraclechain.aos.entity.AmountRecord;
import com.oraclechain.aos.entity.OutAmount;
import com.oraclechain.aos.entity.PurchaseRecords;
import com.oraclechain.aos.entity.usdt.TransactionHashDto;
import com.oraclechain.aos.service.IAmountRecordService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import java.sql.ResultSet;

/**
 * 前端控制器
 *
 * @since 2020-07-21
 */
@EnableAsync
@RestController
@EnableScheduling
@RequestMapping("/AmountRecord")
public class AmountRecordController {

  @Autowired private IAmountRecordService iAmountRecordService;

  // 保存USDT记录
  @Scheduled(fixedRate = 1000*60*3)
  @Async
  public void saveAmountRecord() {
    iAmountRecordService.saveUsdtAmountRecord();
  }

  @Async
  @Scheduled(fixedRate = 1000*60*3)
  public void scan() {
    // 定时查询usdt是否到账
    iAmountRecordService.usdtAmountScan();
  }

  @ApiOperation(value = "分页查询", notes = "分页查询")
  @PostMapping("/page")
  @ResponseBody
  public Result page(@RequestBody PageVo pageVo) {
    Page<AmountRecord> page = new Page<>(pageVo.getPageNum(), pageVo.getPageSize());
    IPage<AmountRecord> amountRecordIPage = iAmountRecordService.page(page);
    return Result.ok(amountRecordIPage);
  }

  @ApiOperation(value = "保存或更新", notes = "保存或更新")
  @PostMapping("/saveOrUpdate")
  public Result saveOrUpdate(@RequestBody AmountRecord amountRecord) {
    return Result.ok(iAmountRecordService.saveOrUpdate(amountRecord));
  }
}

package com.oraclechain.aos.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.oraclechain.aos.common.constant.GlobeConstant;
import com.oraclechain.aos.common.model.PageVo;
import com.oraclechain.aos.common.model.Result;
import com.oraclechain.aos.entity.*;
import com.oraclechain.aos.entity.export.PurchaseExcel;
import com.oraclechain.aos.entity.export.RebateRecordExcel;
import com.oraclechain.aos.entity.param.Record.MyUsdtRecord;
import com.oraclechain.aos.service.IAmountRecordService;
import com.oraclechain.aos.service.IOutAmountService;
import com.oraclechain.aos.service.IPurchaseRecordsService;
import com.oraclechain.aos.service.IRebateRecordService;
import com.oraclechain.aos.utils.ExcelUtil;
import com.oraclechain.aos.utils.StringUtils;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.MapUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 返佣记录 前端控制器
 *
 * @since 2020-07-22
 */
@RestController
@RequestMapping("/RebateRecord")
public class RebateRecordController {

  @Autowired private IRebateRecordService iRebateRecordService;
  @Autowired private IAmountRecordService iAmountRecordService;
  @Autowired private IOutAmountService iOutAmountService;
  @Autowired private IPurchaseRecordsService iPurchaseRecordsService;

  @ApiOperation(value = "审核", notes = "审核")
  @PostMapping("/audit")
  @ResponseBody
  public Result audit(@RequestBody Map<String, Object> params) {
    String id = MapUtils.getString(params, "id");
    RebateRecord rebateRecord = new RebateRecord();
    rebateRecord.setId(id);
    rebateRecord.setIsRelease(2);
    return Result.ok(iRebateRecordService.updateById(rebateRecord));
  }

  @ApiOperation(value = "返佣记录分页", notes = "返佣记录分页")
  @PostMapping("/myAosPage")
  @ResponseBody
  public Result page(@RequestBody PageVo pageVo) {
    SysUser principal = (SysUser) SecurityUtils.getSubject().getPrincipal();
    if (principal == null) {
      return Result.fail("登录失效，请重新登录");
    }
    Page<RebateRecord> page1 = new Page<>(pageVo.getPageNum(), pageVo.getPageSize());
    QueryWrapper<RebateRecord> queryWrapper = new QueryWrapper();
    queryWrapper.eq("type", 1);
    queryWrapper.eq("user_email", principal.getEmail());
    List<RebateRecord> list = iRebateRecordService.list(queryWrapper);
    IPage<RebateRecord> rebateRecords = iRebateRecordService.page(page1, queryWrapper);

    BigDecimal allAmount = new BigDecimal(0);
    for (int i = 0; i < list.size(); i++) {
      allAmount = new BigDecimal(list.get(i).getAmount()).add(allAmount);
    }

    Map<String, Object> result = new HashMap<>();
    // 直接邀请
    result.put("allAmount", allAmount);
    result.put("page", rebateRecords);

    return Result.ok(result);
  }

  @ApiOperation(value = "返佣待发放分页", notes = "返佣待发放分页")
  @PostMapping("/waitAosPage")
  @ResponseBody
  public Result waitAosPage(@RequestBody PageVo pageVo) {
    Page<RebateRecord> page1 = new Page<>(pageVo.getPageNum(), pageVo.getPageSize());
    QueryWrapper<RebateRecord> queryWrapper = new QueryWrapper();
    queryWrapper.eq("type", 1);
    queryWrapper.eq("is_release", 1);
    IPage<RebateRecord> rebateRecords = iRebateRecordService.page(page1, queryWrapper);
    return Result.ok(rebateRecords);
  }

  @ApiOperation(value = "釋放记录分页", notes = "釋放记录分页")
  @PostMapping("/myAosReleasePage")
  @ResponseBody
  public Result myAosReleasePage(@RequestBody PageVo pageVo) {
    SysUser principal = (SysUser) SecurityUtils.getSubject().getPrincipal();
    if (principal == null) {
      return Result.fail("登录失效，请重新登录");
    }
    Page<RebateRecord> page1 = new Page<>(pageVo.getPageNum(), pageVo.getPageSize());
    QueryWrapper<RebateRecord> queryWrapper = new QueryWrapper();
    queryWrapper.eq("type", 3);
    //    List<RebateRecord> list = iRebateRecordService.list(queryWrapper);
    IPage<RebateRecord> rebateRecords = iRebateRecordService.page(page1, queryWrapper);

    return Result.ok(rebateRecords);
  }

  @ApiOperation(value = "返佣记录分页", notes = "返佣记录分页")
  @PostMapping("/myUsdtPage")
  @ResponseBody
  public Result myUsdtPage(@RequestBody @Valid PageVo pageVo) {
    SysUser principal = (SysUser) SecurityUtils.getSubject().getPrincipal();
    Page<RebateRecord> page1 = new Page<>(pageVo.getPageNum(), pageVo.getPageSize());
    QueryWrapper<RebateRecord> queryWrapper = new QueryWrapper();
    queryWrapper.eq("type", 2);
    List<RebateRecord> list = iRebateRecordService.list(queryWrapper);
    IPage<RebateRecord> rebateRecords = iRebateRecordService.page(page1, queryWrapper);
    Map<String, Object> result = new HashMap<>();
    BigDecimal allAmount = new BigDecimal(0);
    for (int i = 0; i < list.size(); i++) {
      allAmount = new BigDecimal(list.get(i).getAmount()).add(allAmount);
    }
    // 直接邀请
    result.put("allAmount", allAmount);
    result.put("page", rebateRecords);
    return Result.ok(result);
  }

  @ApiOperation(value = "list列表", notes = "记录 type 1,返佣 type 3释放 ")
  @PostMapping("/list")
  @ResponseBody
  public Result list(@RequestBody Integer type) {
    List<RebateRecord> rebateRecords =
        iRebateRecordService.list(new QueryWrapper<RebateRecord>().eq("type", type));
    return Result.ok(rebateRecords);
  }

  @ApiOperation(value = "AOS当日待释放", notes = " ")
  @PostMapping("/aosIsReleasePage")
  @ResponseBody
  public Result aosIsReleasePage(@RequestBody PageVo pageVo) {
    Page<RebateRecord> page1 = new Page<>(pageVo.getPageNum(), pageVo.getPageSize());
    // 创建SimpleDateFormat对象，指定样式    2019-05-13 22:39:30
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    Page<RebateRecord> rebateRecords =
        iRebateRecordService.page(
            page1,
            new QueryWrapper<RebateRecord>()
                .eq("is_release", 1)
                .apply("date_format (create_time,'%Y-%m-%d') = {0}", sdf.format(new Date()))
                .eq("type", 3));
    return Result.ok(rebateRecords);
  }

  @ApiOperation(value = "AOS当日待释放", notes = " ")
  @GetMapping("/aosIsReleaseListExcel")
  @ResponseBody
  public void aosIsReleaseListExcel(HttpServletResponse response) throws IOException {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    List<RebateRecord> rebateRecords =
        iRebateRecordService.list(
            new QueryWrapper<RebateRecord>()
                .eq("is_release", 1)
                .apply("date_format (create_time,'%Y-%m-%d') = {0}", sdf.format(new Date()))
                .eq("type", 3));
    ExcelUtil.exportExcel(
        rebateRecords,
        "RebateRecord",
        "RebateRecord",
        RebateRecordExcel.class,
        "RebateRecord",
        response);
  }

  @ApiOperation(value = "返佣记录导出", notes = "返佣记录导出")
  @GetMapping("/export")
  @ResponseBody
  public void export(HttpServletResponse response) throws IOException {
    List<RebateRecord> rebateRecords =
        iRebateRecordService.list(new QueryWrapper<RebateRecord>().eq("type", 1));
    List<RebateRecordExcel> list = new ArrayList<>();
    rebateRecords.stream()
        .forEach(
            rr -> {
              RebateRecordExcel rebateRecordExcel = new RebateRecordExcel();
              rebateRecordExcel.setFromUserId(rr.getFromUserId());
              rebateRecordExcel.setToUserId(rr.getToUserId());
              rebateRecordExcel.setUserEmail(rr.getUserEmail());
              rebateRecordExcel.setAmount(rr.getAmount());
              rebateRecordExcel.setCurrentDay(rr.getCurrentDay());
              rebateRecordExcel.setTotalDay(rr.getTotalDay());
              rebateRecordExcel.setStatus(rr.getStatus().toString());
              rebateRecordExcel.setType(rr.getType().toString());
              list.add(rebateRecordExcel);
            });
    ExcelUtil.exportExcel(
        list, "RebateRecord", "RebateRecord", RebateRecordExcel.class, "RebateRecord", response);
  }

  @ApiOperation(value = "释放记录导出", notes = "释放记录导出")
  @GetMapping("/releaseExport")
  @ResponseBody
  public void releaseExport(HttpServletResponse response) throws IOException {
    List<RebateRecord> rebateRecords =
        iRebateRecordService.list(new QueryWrapper<RebateRecord>().eq("type", 3));
    List<RebateRecordExcel> list = new ArrayList<>();
    rebateRecords.stream()
        .forEach(
            rr -> {
              RebateRecordExcel rebateRecordExcel = new RebateRecordExcel();
              rebateRecordExcel.setFromUserId(rr.getFromUserId());
              rebateRecordExcel.setToUserId(rr.getToUserId());
              rebateRecordExcel.setUserEmail(rr.getUserEmail());
              rebateRecordExcel.setAmount(rr.getAmount());
              rebateRecordExcel.setCurrentDay(rr.getCurrentDay());
              rebateRecordExcel.setTotalDay(rr.getTotalDay());
              rebateRecordExcel.setStatus(rr.getStatus().toString());
              rebateRecordExcel.setType(rr.getType().toString());
              list.add(rebateRecordExcel);
            });
    ExcelUtil.exportExcel(
        list, "RebateRecord", "RebateRecord", RebateRecordExcel.class, "RebateRecord", response);
  }

  @ApiOperation(value = "返佣记录", notes = "返佣记录出")
  @PostMapping("/myUsdt")
  @ResponseBody
  public Result myUsdt(@RequestHeader("Language") String language) {
    SysUser principal = (SysUser) SecurityUtils.getSubject().getPrincipal();
    List<RebateRecord> rebateRecords =
        iRebateRecordService.list(
            new QueryWrapper<RebateRecord>().eq("type", "2").eq("to_user_id", principal.getId()));
    ArrayList<MyUsdtRecord> myUsdtRecords = new ArrayList<>();
    for (int i = 0; i < rebateRecords.size(); i++) {
      MyUsdtRecord myUsdtRecord = new MyUsdtRecord();
      myUsdtRecord.setAmount(rebateRecords.get(i).getAmount());
      myUsdtRecord.setCreateTime(rebateRecords.get(i).getCreateTime());
      if (language.equals(GlobeConstant.CHINESE)){myUsdtRecord.setType(GlobeConstant.StraightCommission_Chinese);}
      if (language.equals(GlobeConstant.ENGLISH)){ myUsdtRecord.setType(GlobeConstant.StraightCommission_English);}
      if (language.equals(GlobeConstant.KOREAN)){ myUsdtRecord.setType(GlobeConstant.StraightCommission_Korean);}
      myUsdtRecord.setSymbol(1);
      myUsdtRecords.add(myUsdtRecord);
    }
    List<AmountRecord> amountRecords =
        iAmountRecordService.list(
            new QueryWrapper<AmountRecord>()
                .eq("type", "2")
                .eq("status", 1)
                .eq("user_id", principal.getId()));
    for (int i = 0; i < amountRecords.size(); i++) {
      MyUsdtRecord myUsdtRecord = new MyUsdtRecord();
      myUsdtRecord.setAmount(Long.valueOf(amountRecords.get(i).getUsdtAmount()));
      myUsdtRecord.setCreateTime(amountRecords.get(i).getCreateTime());
      myUsdtRecord.setSymbol(1);
      if (language.equals(GlobeConstant.CHINESE)){myUsdtRecord.setType(GlobeConstant.BeRechargedSuccessfully_Chinese);}
      if (language.equals(GlobeConstant.ENGLISH)){ myUsdtRecord.setType(GlobeConstant.BeRechargedSuccessfully_English);}
      if (language.equals(GlobeConstant.KOREAN)){ myUsdtRecord.setType(GlobeConstant.BeRechargedSuccessfully_Korean);}
      myUsdtRecords.add(myUsdtRecord);
    }
    List<PurchaseRecords> purchaseRecords =
        iPurchaseRecordsService.list(
            new QueryWrapper<PurchaseRecords>().eq("user_id", principal.getId()));
    for (int i = 0; i < purchaseRecords.size(); i++) {
      MyUsdtRecord myUsdtRecord = new MyUsdtRecord();
      myUsdtRecord.setAmount(purchaseRecords.get(i).getUsdtAmountCosts());
      myUsdtRecord.setCreateTime(purchaseRecords.get(i).getCreateTime());
      if (language.equals(GlobeConstant.CHINESE)){myUsdtRecord.setType(GlobeConstant.PurchaseCost_Chinese);}
      if (language.equals(GlobeConstant.ENGLISH)){ myUsdtRecord.setType(GlobeConstant.PurchaseCost_English);}
      if (language.equals(GlobeConstant.KOREAN)){ myUsdtRecord.setType(GlobeConstant.PurchaseCost_Korean);}
      myUsdtRecord.setSymbol(0);
      myUsdtRecords.add(myUsdtRecord);
    }

    List<OutAmount> outAmounts =
        iOutAmountService.list(
            new QueryWrapper<OutAmount>()
                .eq("user_id", principal.getId())
                .eq("audit", 1)
                .eq("type", 2));
    for (int i = 0; i < outAmounts.size(); i++) {
      MyUsdtRecord myUsdtRecord = new MyUsdtRecord();
      myUsdtRecord.setAmount(outAmounts.get(i).getAmount());
      myUsdtRecord.setCreateTime(outAmounts.get(i).getCreateTime());
      if (language.equals(GlobeConstant.CHINESE)){myUsdtRecord.setType(GlobeConstant.MentionMoneySuccess_Chinese);}
      if (language.equals(GlobeConstant.ENGLISH)){ myUsdtRecord.setType(GlobeConstant.MentionMoneySuccess_English);}
      if (language.equals(GlobeConstant.KOREAN)){ myUsdtRecord.setType(GlobeConstant.MentionMoneySuccess_Korean);}
      myUsdtRecord.setSymbol(0);
      myUsdtRecords.add(myUsdtRecord);
    }
    return Result.ok(myUsdtRecords);
  }

  @ApiOperation(value = "返佣记录", notes = "返佣记录出")
  @PostMapping("/myAos")
  @ResponseBody
  public Result myAos(@RequestHeader("Language") String language) {
    SysUser principal = (SysUser) SecurityUtils.getSubject().getPrincipal();
    List<RebateRecord> rebateRecords =
        iRebateRecordService.list(
            new QueryWrapper<RebateRecord>()
                .eq("type", "3")
                .eq("is_release", 3)
                .eq("to_user_id", principal.getId()));
    ArrayList<MyUsdtRecord> myUsdtRecords = new ArrayList<>();
    for (int i = 0; i < rebateRecords.size(); i++) {
      MyUsdtRecord myUsdtRecord = new MyUsdtRecord();
      myUsdtRecord.setAmount(rebateRecords.get(i).getAmount());
      myUsdtRecord.setCreateTime(rebateRecords.get(i).getCreateTime());
      if (language.equals(GlobeConstant.CHINESE)){myUsdtRecord.setType(GlobeConstant.ReleaseSuccess_Chinese);}
      if (language.equals(GlobeConstant.ENGLISH)){ myUsdtRecord.setType(GlobeConstant.ReleaseSuccess_English);}
      if (language.equals(GlobeConstant.KOREAN)){ myUsdtRecord.setType(GlobeConstant.ReleaseSuccess_Korean);}
      myUsdtRecord.setSymbol(1);
      myUsdtRecords.add(myUsdtRecord);
    }

    List<RebateRecord> rebateRecordList =
        iRebateRecordService.list(
            new QueryWrapper<RebateRecord>().eq("type", "1").eq("to_user_id", principal.getId()));
    for (int i = 0; i < rebateRecordList.size(); i++) {
      MyUsdtRecord myUsdtRecord = new MyUsdtRecord();
      myUsdtRecord.setAmount(rebateRecordList.get(i).getAmount());
      myUsdtRecord.setCreateTime(rebateRecordList.get(i).getCreateTime());
      if (language.equals(GlobeConstant.CHINESE)){myUsdtRecord.setType(GlobeConstant.CommissionIncome_Chinese);}
      if (language.equals(GlobeConstant.ENGLISH)){ myUsdtRecord.setType(GlobeConstant.CommissionIncome_English);}
      if (language.equals(GlobeConstant.KOREAN)){ myUsdtRecord.setType(GlobeConstant.CommissionIncome_Korean);}
      myUsdtRecord.setSymbol(1);
      myUsdtRecords.add(myUsdtRecord);
    }

    List<OutAmount> outAmounts =
        iOutAmountService.list(
            new QueryWrapper<OutAmount>()
                .eq("user_id", principal.getId())
                .eq("type", 1)
                .eq("status", 1));
    for (int i = 0; i < outAmounts.size(); i++) {
      MyUsdtRecord myUsdtRecord = new MyUsdtRecord();
      myUsdtRecord.setAmount(outAmounts.get(i).getAmount());
      myUsdtRecord.setCreateTime(outAmounts.get(i).getCreateTime());
      if (language.equals(GlobeConstant.CHINESE)){myUsdtRecord.setType(GlobeConstant.MentionMoneySuccess_Chinese);}
      if (language.equals(GlobeConstant.ENGLISH)){ myUsdtRecord.setType(GlobeConstant.MentionMoneySuccess_English);}
      if (language.equals(GlobeConstant.KOREAN)){ myUsdtRecord.setType(GlobeConstant.MentionMoneySuccess_Korean);}
      myUsdtRecord.setSymbol(0);
      myUsdtRecords.add(myUsdtRecord);
    }
    return Result.ok(myUsdtRecords);
  }

  @ApiOperation(value = "打款到用户账户", notes = "打款到用户账户")
  @Scheduled(fixedRate = 1000 * 60 * 60)
  public void toAccount() {
    iRebateRecordService.toAccount();
  }
}

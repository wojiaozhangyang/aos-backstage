package com.oraclechain.aos.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.oraclechain.aos.common.model.PageVo;
import com.oraclechain.aos.common.model.Result;
import com.oraclechain.aos.entity.PurchaseRecords;
import com.oraclechain.aos.entity.SysUser;
import com.oraclechain.aos.entity.export.PurchaseExcel;
import com.oraclechain.aos.entity.param.Record.AddPurchaseVo;
import com.oraclechain.aos.service.IPurchaseRecordsService;
import com.oraclechain.aos.service.ISysRoleUserService;
import com.oraclechain.aos.utils.ExcelUtil;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 申购记录 前端控制器
 *
 * @since 2020-07-16
 */
@EnableScheduling
@RestController
@RequestMapping("/Purchase")
public class PurchaseRecordsController {

  @Autowired private IPurchaseRecordsService iPurchaseRecordsService;

  @ApiOperation(value = "剩余申购额度", notes = "剩余额度")
  @PostMapping("/remainingAmount")
  @ResponseBody
  public Result remainingAmount() {
    Long remainingAmount = iPurchaseRecordsService.remainingAmount();
    return Result.ok(remainingAmount.toString(), "查询成功");
  }

  @ApiOperation(value = "分页查询", notes = "分页查询")
  @PostMapping("/page")
  @ResponseBody
  public Result page(@RequestBody PageVo pageVo) {
    Page<PurchaseRecords> page = new Page<>(pageVo.getPageNum(), pageVo.getPageSize());
    IPage<PurchaseRecords> purchaseRecordsPage = iPurchaseRecordsService.page(page);
    return Result.ok(purchaseRecordsPage);
  }

  @ApiOperation(value = "分页查询", notes = "分页查询")
  @PostMapping("/currentPage")
  @ResponseBody
  public Result currentPage(@RequestBody PageVo pageVo) {
    SysUser principal = (SysUser) SecurityUtils.getSubject().getPrincipal();
    Page<PurchaseRecords> page = new Page<>(pageVo.getPageNum(), pageVo.getPageSize());
    IPage<PurchaseRecords> purchaseRecordsPage =
        iPurchaseRecordsService.page(
            page, new QueryWrapper<PurchaseRecords>().eq("user_id", principal.getId()));
    return Result.ok(purchaseRecordsPage);
  }

  @ApiOperation(value = "list查询", notes = "list查询")
  @PostMapping("/list")
  @ResponseBody
  public Result list() {
    List<PurchaseRecords> purchaseRecordsPage = iPurchaseRecordsService.list();
    return Result.ok(purchaseRecordsPage);
  }

  @ApiOperation(value = "申购申请", notes = "保存或更新")
  @PostMapping("/addPurchase")
  public Result addPurchase(@RequestHeader("Language") String language ,@RequestBody AddPurchaseVo addPurchaseVo) {
    return iPurchaseRecordsService.addPurchase(language,addPurchaseVo);
  }

  @ApiOperation(value = "保存或更新", notes = "保存或更新")
  @PostMapping("/saveOrUpdate")
  public Result saveOrUpdate(@RequestBody PurchaseRecords purchaseRecords) {
    return Result.ok(iPurchaseRecordsService.saveOrUpdate(purchaseRecords));
  }

  @ApiOperation(value = "返佣审核", notes = "返佣审核")
  @PostMapping("/timingCommissionAudit")
  public Result rebort(@RequestBody String[] ids) {
    iPurchaseRecordsService.TimingCommission();
    for (int i = 0; i < ids.length; i++) {
      PurchaseRecords purchaseRecords = new PurchaseRecords();
      purchaseRecords.setId(ids[i]);
      purchaseRecords.setIsAmount(0);
      iPurchaseRecordsService.updateById(purchaseRecords);
    }
    return Result.ok("审核成功");
  }

  @ApiOperation(value = "定时返aos", notes = "定时返aos")
  @Scheduled(fixedRate = 1000 * 60 * 60 * 24)
  public void TimingCommission() {
    iPurchaseRecordsService.TimingCommission();
  }

  @ApiOperation(value = "定时更新状态", notes = "定时更新状态")
  @Scheduled(fixedRate = 1200 * 60 * 60 * 24)
  public void TimingCommissionState() {
    List<PurchaseRecords> list =
        iPurchaseRecordsService.list(new QueryWrapper<PurchaseRecords>().eq("is_amount", 0));
    list.stream()
        .forEach(
            lis -> {
              if (lis.getAosActualDay() >= lis.getAosActualCount()) {
                lis.setIsAmount(1);
                iPurchaseRecordsService.updateById(lis);
              }
            });
  }

  @ApiOperation(value = "申购记录导出", notes = "申购记录导出")
  @GetMapping("/export")
  @ResponseBody
  public void export(HttpServletResponse response) throws IOException {
    List<PurchaseRecords> purchaseRecordsPage = iPurchaseRecordsService.list();
    List<PurchaseExcel> purchaseExcels = new ArrayList<>();
    for (int i=0;i<purchaseRecordsPage.size();i++){
      PurchaseExcel purchaseExcel =new PurchaseExcel();
      purchaseExcel.setAosActualCount(purchaseRecordsPage.get(i).getAosActualCount());
      purchaseExcel.setAosActualDay(purchaseRecordsPage.get(i).getAosActualDay());
      purchaseExcel.setExtraPredictCount(purchaseRecordsPage.get(i).getExtraPredictCount());
      purchaseExcel.setExtraPredictDay(purchaseRecordsPage.get(i).getExtraPredictDay());
      purchaseExcel.setUsdtAmountCosts(purchaseRecordsPage.get(i).getUsdtAmountCosts());
      purchaseExcel.setUserId(purchaseRecordsPage.get(i).getUserId());
      purchaseExcels.add(purchaseExcel);
    }

    ExcelUtil.exportExcel(
            purchaseExcels, "purchaseRecord", "purchaseRecord", PurchaseExcel.class, "purchaseRecord", response);
  }
}

package com.oraclechain.aos.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @since 2020-08-06
 */
@RestController
@RequestMapping("/SysWalletInfo")
public class SysWalletInfoController {

}

package com.oraclechain.aos.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.oraclechain.aos.common.constant.GlobeConstant;
import com.oraclechain.aos.common.model.PageVo;
import com.oraclechain.aos.common.model.Result;
import com.oraclechain.aos.entity.OutAmount;
import com.oraclechain.aos.entity.SysUser;
import com.oraclechain.aos.entity.UserAccount;
import com.oraclechain.aos.service.IOutAmountService;
import com.oraclechain.aos.service.IUserAccountService;
import com.oraclechain.aos.utils.RuleUtils;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;
import java.math.BigDecimal;

/**
 * 前端控制器
 *
 * @since 2020-07-23
 */
@EnableAsync
@EnableScheduling
@RestController
@RequestMapping("/OutAmount")
public class OutAmountController {

  @Autowired private IUserAccountService iUserAccountService;
  @Autowired private IOutAmountService iOutAmountService;




  @ApiOperation(value = "保存或更新", notes = "保存或更新")
  @PostMapping("/saveOrUpdate")
  public Result saveOrUpdate(@RequestBody OutAmount outAmount) {
    return Result.ok(iOutAmountService.saveOrUpdate(outAmount));
  }

  @ApiOperation(value = "aos分页查询", notes = "分页查询")
  @PostMapping("/aosPage")
  @ResponseBody
  public Result aosPage(@RequestBody PageVo pageVo){
    Page<OutAmount> page = new Page<>(pageVo.getPageNum(),pageVo.getPageSize());
    IPage<OutAmount> outAmountIPage = iOutAmountService.page(page,new QueryWrapper<OutAmount>().eq("type",1).eq("audit",0));
    return  Result.ok(outAmountIPage);
  }
  @ApiOperation(value = "usdt分页查询", notes = "分页查询")
  @PostMapping("/usdtPage")
  @ResponseBody
  public Result usdtPage(@RequestBody PageVo pageVo){
    Page<OutAmount> page = new Page<>(pageVo.getPageNum(),pageVo.getPageSize());
    IPage<OutAmount> outAmountIPage = iOutAmountService.page(page,new QueryWrapper<OutAmount>().eq("type",2).eq("audit",0));
    return  Result.ok(outAmountIPage);
  }

  @PostMapping("/usdtPutMoney")
  @ApiOperation(value = "用户发起usdt提币", notes = "用户发起usdt提币")
  public Result usdtPutMoney(@RequestHeader("Language") String language,@RequestBody OutAmount outAmount) {
    if (!RuleUtils.isUsdtAddres(outAmount.getToAddress())){
      if (language.equals(GlobeConstant.CHINESE)){ return Result.fail(GlobeConstant.IrregularAddress_Chinese);}
      if (language.equals(GlobeConstant.ENGLISH)){ return Result.fail(GlobeConstant.IrregularAddress_English);}
      if (language.equals(GlobeConstant.KOREAN)){ return Result.fail(GlobeConstant.IrregularAddress_Korean);}

    }
    if (outAmount.getProcedureFee()>outAmount.getAmount()){
      if (language.equals(GlobeConstant.CHINESE)){ return Result.fail(GlobeConstant.WithdrawalAmountShouldGreaterHandlingFee_Chinese);}
      if (language.equals(GlobeConstant.ENGLISH)){ return Result.fail(GlobeConstant.WithdrawalAmountShouldGreaterHandlingFee_English);}
      if (language.equals(GlobeConstant.KOREAN)){ return Result.fail(GlobeConstant.WithdrawalAmountShouldGreaterHandlingFee_Korean);}
    }
    // 获取当前用户信息
    SysUser principal = (SysUser) SecurityUtils.getSubject().getPrincipal();
    UserAccount userAccount =
            iUserAccountService.getOne(
                    new QueryWrapper<UserAccount>().eq("user_id", principal.getId()));
    if (Long.valueOf(userAccount.getUsdtBalance()) < Long.valueOf(outAmount.getAmount())){
      if (language.equals(GlobeConstant.CHINESE)){ return Result.fail(GlobeConstant.TheSubscriptionAmountIsLargerThanAccountAmount_Chinese);}
      if (language.equals(GlobeConstant.ENGLISH)){ return Result.fail(GlobeConstant.TheSubscriptionAmountIsLargerThanAccountAmount_English);}
      if (language.equals(GlobeConstant.KOREAN)){ return Result.fail(GlobeConstant.TheSubscriptionAmountIsLargerThanAccountAmount_Korean);}
    }


    return Result.ok(iOutAmountService.usdtOutAmount(outAmount));
  }

  @PostMapping("/aosPutMoney")
  @ApiOperation(value = "用户发起aos提币", notes = "用户发起aos提币")
  public Result aosPutMoney(@RequestHeader("Language") String language,@RequestBody OutAmount outAmount) {
    if (!RuleUtils.isAosAddres(outAmount.getToAddress())){
      if (language.equals(GlobeConstant.CHINESE)){ return Result.fail(GlobeConstant.IrregularAddress_Chinese);}
      if (language.equals(GlobeConstant.ENGLISH)){ return Result.fail(GlobeConstant.IrregularAddress_English);}
      if(language.equals(GlobeConstant.KOREAN)){ return Result.fail(GlobeConstant.IrregularAddress_Korean);}
      return null;
    }else {
      return Result.ok(iOutAmountService.aosPutMoney(outAmount));
    }
  }

  @PostMapping("/audit")
  @ApiOperation(value = "审核提币", notes = "审核提币")
  public Result audit(@RequestBody OutAmount outAmount) {
    OutAmount outAmount1 = new OutAmount();
    outAmount1.setId(outAmount.getId());
    outAmount1.setAudit(1);
    boolean b = iOutAmountService.updateById(outAmount1);
    if (b) {
      // 减去账户钱
      OutAmount byId = iOutAmountService.getById(outAmount.getId());
      UserAccount userAccount =
              iUserAccountService.getOne(
                      new QueryWrapper<UserAccount>().eq("user_id", byId.getUserId()));
      if (byId.getType() == 2) {
        BigDecimal amount = new BigDecimal(byId.getAmount());
        userAccount.setUsdtBalance(new BigDecimal(userAccount.getUsdtBalance()).subtract(amount).longValue());
      }else {
        userAccount.setAosBalance(new BigDecimal(userAccount.getAosBalance()).subtract(new BigDecimal(byId.getAmount())).longValue());
      }
      iUserAccountService.updateById(userAccount);
    }
    return Result.ok(b);
  }


  @ApiOperation(value = "定时任务", notes = "检查提币审核成功给打款")
  @Scheduled(fixedRate = 1000*60)
  public void PutForwardTheAccount() {
    iOutAmountService.putForwardTheAccount();
  }


  @ApiOperation(value = "定时任务USDT提币检查状态", notes = "检查状态")
  @Scheduled(fixedRate = 1000*60)
  public void checkUsdtStatus() {
    iOutAmountService.checkUsdtStatus();
  }


  @ApiOperation(value = "定时任务AOS提币检查状态", notes = "检查状态")
  @Scheduled(fixedRate = 1000*60)
  public void checkAosStatus() {
    iOutAmountService.checkAosStatus();
  }



}

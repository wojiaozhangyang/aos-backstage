package com.oraclechain.aos.controller;


import com.oraclechain.aos.common.model.Result;
import com.oraclechain.aos.entity.SysRole;
import com.oraclechain.aos.service.ISysRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @since 2020-07-14
 */
@Slf4j
@RestController
@RequestMapping("/SysRole")
public class SysRoleController {

    @Autowired
    private ISysRoleService iSysRoleService;


    /**
     * 角色新增或者更新
     *
     * @param sysRole
     * @return
     */
    @PostMapping("/roles/saveOrUpdate")
    public Result saveOrUpdate(@RequestBody @Valid SysRole sysRole) {
        return Result.ok(iSysRoleService.saveOrUpdate(sysRole));
    }




}

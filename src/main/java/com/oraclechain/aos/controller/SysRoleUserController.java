package com.oraclechain.aos.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户角色中间表 前端控制器
 * </p>
 *
 * @since 2020-07-14
 */
@RestController
@RequestMapping("/SysRoleUser")
public class SysRoleUserController {

}

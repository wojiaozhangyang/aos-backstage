package com.oraclechain.aos.utils;

/**
 * @ClassName: RulesUtils @Description: Todo
 *
 * @data: 2020/8/3 21:13
 */
public class RuleUtils {

  public static boolean isAosAddres(String address) {
      if (StringUtils.isNotEmpty(address) && address.length() == 12) {
          return true;
      }
    return  false;
  }

  public static boolean isUsdtAddres(String address) {
    if (StringUtils.isNull(address) && !address.startsWith("0x")) {
      return false;
    }
    return true;
  }
}

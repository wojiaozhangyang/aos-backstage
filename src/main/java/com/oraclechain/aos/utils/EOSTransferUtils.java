package com.oraclechain.aos.utils;

/**
 * @ClassName: OfflineEOSTransfer
 * @Description: Todo
 * @data: 2020/7/23  10:49
 */


import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.oraclechain.aos.blockchain.PushDatamanger;
import com.oraclechain.aos.blockchain.bean.PushSuccessBean;
import com.oraclechain.aos.common.constant.GlobeConstant;
import com.oraclechain.aos.entity.aos.Processed;
import com.oraclechain.aos.entity.aos.TransferEosMessageBean;
import io.jafka.jeos.EosApi;
import io.jafka.jeos.EosApiFactory;
import io.jafka.jeos.LocalApi;
import io.jafka.jeos.core.common.SignArg;
import io.jafka.jeos.core.request.chain.transaction.PushTransactionRequest;
import io.jafka.jeos.core.response.chain.transaction.PushedTransaction;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

/**
 *
 * @author adyliu (imxylz@gmail.com)
 * @since Sep 28, 2018
 */
@Slf4j
public class EOSTransferUtils {


    public static void main(String[] args) throws Exception{

        // --- get the current state of blockchain
        EosApi eosApi = EosApiFactory.create("https://api.kylin.alohaeos.com");
        SignArg arg = eosApi.getSignArg(120);
        System.out.println(eosApi.getObjectMapper().writeValueAsString(arg));

        // --- sign the transation of token tansfer
        String privateKey = "5KDWuL82BBJUJbG9xKqq9XUtQb42riaanLA2NZK8CTiDpaB8pRT";//replace the real private key
        String from = "1111";
        String to = "eosio";
        String quantity = "0.0001 EOS";
        String memo = "sent by eos sdk (https://github.com/adyliu/jeos";
        LocalApi localApi = EosApiFactory.createLocalApi();
        PushTransactionRequest req = localApi.transfer(arg, privateKey, from, to, quantity, memo);
        System.out.println(localApi.getObjectMapper().writeValueAsString(req));


        // --- push the signed-transaction to the blockchain
        PushedTransaction pts = eosApi.pushTransaction(req);
        System.out.println(localApi.getObjectMapper().writeValueAsString(pts));


    }


    public static  String getEOSTransfer(String from,String to,String quantity) throws Exception{

        final String[] transactionId = new String[1];
        new PushDatamanger(new PushDatamanger.Callback() {
            @Override
            public void getResult(PushSuccessBean.DataBeanX pushSuccessBean) {
                transactionId[0] = pushSuccessBean.getTransaction_id();
                log.info(transactionId[0]);

            }
        }).pushAction(GlobeConstant.CONTRACT_ADDRESS,"transfer",new Gson().toJson(new TransferEosMessageBean("",to,quantity,from)),"aaaaaaaaaaaaa");




        return transactionId[0];
    }






}
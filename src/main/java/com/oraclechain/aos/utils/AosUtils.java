package com.oraclechain.aos.utils;

import cn.hutool.http.HttpRequest;
import com.alibaba.fastjson.JSONObject;
import com.ejlchina.okhttps.HTTP;
import com.ejlchina.okhttps.HttpResult;
import com.ejlchina.okhttps.OkHttps;
import com.oraclechain.aos.entity.aos.BlockDto;
import com.oraclechain.aos.entity.aos.BlockInfoDto;
import com.oraclechain.aos.entity.aos.TransactionDto;
import lombok.extern.slf4j.Slf4j;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @ClassName: AosUtils @Description: Todo
 *
 * @data: 2020/7/30 16:22
 */
@Slf4j
public class AosUtils {

  private volatile ReentrantLock lock = new ReentrantLock();

  public static void main(String[] args) {
   //getInfo();
   // getBlock();
    getTransaction("e72cb09a6308f5d7f9c3fc618963b57711143405ae455f865a32bd29ee92fb86");
  }

  public static BlockInfoDto getInfo() {
    HttpResult.Body body =
        OkHttps.sync("https://api.kylin.alohaeos.com/v1/chain/get_info").get().getBody();
    // 解析
    BlockInfoDto blockDto = JSONObject.parseObject(body.toString(), BlockInfoDto.class);
    System.out.println("------------" + blockDto.toString());
      System.out.println("---------**************************---" );
    return blockDto;
  }

  public static BlockDto getBlock() {
    HttpResult block_num_or_id = OkHttps.async("https://api-kylin.eosasia.one/v1/chain/get_block")
            .addHeader("Content-Type", "application/json")
            .addBodyPara("block_num_or_id", "116802014").post().getResult();


    log.info(String.valueOf(block_num_or_id));
    // 解析
    //BlockDto blockDto = JSONObject.parseObject(body.toString(), BlockDto.class);
    //System.out.println("------------" + block_num_or_id.toString());
    return null;
  }


  public static Boolean getTransaction(String id) {
    JSONObject jsonObject = new JSONObject();
    jsonObject.put("id", id);
    String jsonStr = JSONObject.toJSONString(jsonObject);
    //链式构建请求
    String result = HttpRequest.post("http://api.aos.plus:8888/v1/history/get_transaction")
            .body(jsonStr)//表单内容
            //超时，毫秒
            .timeout(30000)
            .execute().body();

    TransactionDto transactionDto = JSONObject.parseObject(result, TransactionDto.class);
    log.info(Long.toString(transactionDto.getBlock_num()));
    log.info(Long.toString(transactionDto.getLast_irreversible_block()));
    if ((transactionDto.getBlock_num()).longValue()<transactionDto.getLast_irreversible_block()){
      return true;
    }

    return false;

  }


}

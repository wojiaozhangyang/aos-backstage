package com.oraclechain.aos.utils;

/**
 * @ClassName: ColdWallet @Description: Todo
 *
 * @data: 2020/7/23 19:16
 */
import com.alibaba.fastjson.JSONObject;
import com.ejlchina.okhttps.HTTP;
import com.ejlchina.okhttps.HttpResult;
import com.ejlchina.okhttps.HttpTask;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.oraclechain.aos.common.constant.GlobeConstant;
import com.oraclechain.aos.entity.usdt.TransactionHashDto;
import com.oraclechain.aos.entity.usdt.UsdtResultDto;
import okhttp3.ConnectionPool;
import okhttp3.OkHttpClient;
import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Bool;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.*;
import org.web3j.protocol.ObjectMapperFactory;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.response.EthGetTransactionCount;
import org.web3j.protocol.core.methods.response.EthSendTransaction;
import org.web3j.tx.ChainId;
import org.web3j.utils.Convert;
import org.web3j.utils.Numeric;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.*;
import java.util.concurrent.TimeUnit;

/** 冷钱包 账号 交易相关 */

public class ColdWallet {


  public static String transaction(
      String from, String to, Long amount, BigInteger gas, String privateKey) {
    HTTP http =
        HTTP.builder()
            .config(
                (OkHttpClient.Builder builder) -> {
                  builder.connectionPool(new ConnectionPool(10, 5, TimeUnit.MINUTES));
                  builder.connectTimeout(30, TimeUnit.SECONDS);
                  builder.callTimeout(30,TimeUnit.SECONDS);
                  builder.writeTimeout(30, TimeUnit.SECONDS);
                  builder.readTimeout(30, TimeUnit.SECONDS);
                  builder.retryOnConnectionFailure(true);
                  builder.pingInterval(60, TimeUnit.SECONDS);
                })
                .exceptionListener((HttpTask<?> task, IOException error) -> {
                  task.nothrow();
                  return true;
                })
            .build();
    BigInteger nonce;
    HttpResult result =
        http.async(GlobeConstant.ETH_INTERFACE_ADDRESS)
            .addUrlPara("apikey", GlobeConstant.EasyApiKay)
            .addUrlPara("tag", "latest")
            .addUrlPara("address", from)
            .addUrlPara("action", "eth_getTransactionCount")
            .addUrlPara("module", "proxy")
            .post()
            .getResult();
    Gson gson = new Gson();
    UsdtResultDto usdtResultDto = gson.fromJson(result.getBody().toString(), UsdtResultDto.class);
    String ethGetTransactionCount = usdtResultDto.getResult();

    if (ethGetTransactionCount == null) {
      return null;
    }

    if (ethGetTransactionCount.startsWith("0x")) {
      ethGetTransactionCount = ethGetTransactionCount.substring(2);
    }
    nonce = BigInteger.valueOf(Long.parseLong(ethGetTransactionCount, 16));
    BigInteger gasPrice = Convert.toWei(String.valueOf(gas), Convert.Unit.GWEI).toBigInteger();

    Function function =
        new Function(
            "transfer",
            Arrays.asList(new Address(to), new Uint256(amount)),
            Arrays.asList(new TypeReference<Type>() {}));
    String encodedFunction = FunctionEncoder.encode(function);
    BigInteger gasLimit = BigInteger.valueOf(60000);

    String signedData;
    RawTransaction rawTransaction =
        RawTransaction.createTransaction(
            nonce, gasPrice, gasLimit, GlobeConstant.CONTRACT_ADDRESS, encodedFunction);
    if (privateKey.startsWith("0x")) {
      privateKey = privateKey.substring(2);
    }
    ECKeyPair ecKeyPair = ECKeyPair.create(new BigInteger(privateKey, 16));
    Credentials credentials = Credentials.create(ecKeyPair);
    byte[] signedMessage = TransactionEncoder.signMessage(rawTransaction, credentials);
    signedData = Numeric.toHexString(signedMessage);
    Map<String, String> map = new HashMap();
    if (signedData != null) {
      HttpResult usdtResultResult =
          http.async(GlobeConstant.ETH_INTERFACE_ADDRESS)
              .addUrlPara("apikey", GlobeConstant.EasyApiKay)
              .addUrlPara("hex", signedData)
              .addUrlPara("action", "eth_sendRawTransaction")
              .addUrlPara("module", "proxy")
              .post()
              .getResult();

      TransactionHashDto usdtResultDto1 = JSONObject.parseObject(usdtResultResult.getBody().toString(), TransactionHashDto.class);
      String sendHash = usdtResultDto1.getResult();
      return sendHash;
    } else {
      return null;
    }
  }


  private static void testTokenTransaction(
      Web3j web3j,
      String fromAddress,
      String privateKey,
      String contractAddress,
      String toAddress,
      double amount,
      int decimals) {
    BigInteger nonce;
    EthGetTransactionCount ethGetTransactionCount = null;
    try {
      ethGetTransactionCount =
          web3j.ethGetTransactionCount(fromAddress, DefaultBlockParameterName.PENDING).send();
    } catch (IOException e) {
      e.printStackTrace();
    }
    if (ethGetTransactionCount == null) {
      return;
    }
    nonce = ethGetTransactionCount.getTransactionCount();
    System.out.println("nonce " + nonce);
    BigInteger gasPrice = Convert.toWei(BigDecimal.valueOf(3), Convert.Unit.GWEI).toBigInteger();
    BigInteger gasLimit = BigInteger.valueOf(60000);
    BigInteger value = BigInteger.ZERO;
    // token转账参数
    String methodName = "transfer";
    List<Type> inputParameters = new ArrayList<>();
    List<TypeReference<?>> outputParameters = new ArrayList<>();
    Address tAddress = new Address(toAddress);
    Uint256 tokenValue =
        new Uint256(
            BigDecimal.valueOf(amount).multiply(BigDecimal.TEN.pow(decimals)).toBigInteger());
    inputParameters.add(tAddress);
    inputParameters.add(tokenValue);
    TypeReference<Bool> typeReference = new TypeReference<Bool>() {};
    outputParameters.add(typeReference);
    Function function = new Function(methodName, inputParameters, outputParameters);
    String data = FunctionEncoder.encode(function);

    byte chainId = ChainId.NONE;
    String signedData;
    try {
      signedData =
          ColdWallet.signTransaction(
              nonce, gasPrice, gasLimit, contractAddress, value, data, chainId, privateKey);
      if (signedData != null) {
        EthSendTransaction ethSendTransaction = web3j.ethSendRawTransaction(signedData).send();
        System.out.println(ethSendTransaction.getTransactionHash());
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * 创建钱包
   *
   * @param password 密码
   */
  public static void createWallet(String password)
      throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException,
          CipherException, JsonProcessingException {
    WalletFile walletFile;
    ECKeyPair ecKeyPair = Keys.createEcKeyPair();
    walletFile = Wallet.createStandard(password, ecKeyPair);
    System.out.println("address " + walletFile.getAddress());
    ObjectMapper objectMapper = ObjectMapperFactory.getObjectMapper();
    String jsonStr = objectMapper.writeValueAsString(walletFile);
    System.out.println("keystore json file " + jsonStr);
  }

  /**
   * 解密keystore 得到私钥
   *
   * @param keystore
   * @param password
   */
  public static String decryptWallet(String keystore, String password) {
    String privateKey = null;
    ObjectMapper objectMapper = ObjectMapperFactory.getObjectMapper();
    try {
      WalletFile walletFile = objectMapper.readValue(keystore, WalletFile.class);
      ECKeyPair ecKeyPair = null;
      ecKeyPair = Wallet.decrypt(password, walletFile);
      privateKey = ecKeyPair.getPrivateKey().toString(16);
      System.out.println(privateKey);
    } catch (CipherException e) {
      if ("Invalid password provided".equals(e.getMessage())) {
        System.out.println("密码错误");
      }
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return privateKey;
  }

  /** 签名交易 */
  public static String signTransaction(
      BigInteger nonce,
      BigInteger gasPrice,
      BigInteger gasLimit,
      String to,
      BigInteger value,
      String data,
      byte chainId,
      String privateKey)
      throws IOException {
    byte[] signedMessage;
    RawTransaction rawTransaction =
        RawTransaction.createTransaction(nonce, gasPrice, gasLimit, to, value, data);

    if (privateKey.startsWith("0x")) {
      privateKey = privateKey.substring(2);
    }
    ECKeyPair ecKeyPair = ECKeyPair.create(new BigInteger(privateKey, 16));
    Credentials credentials = Credentials.create(ecKeyPair);

    if (chainId > ChainId.MAINNET) {
      signedMessage = TransactionEncoder.signMessage(rawTransaction, chainId, credentials);
    } else {
      signedMessage = TransactionEncoder.signMessage(rawTransaction, credentials);
    }

    String hexValue = Numeric.toHexString(signedMessage);
    return hexValue;
  }
}

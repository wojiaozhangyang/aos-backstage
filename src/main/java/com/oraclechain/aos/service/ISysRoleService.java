package com.oraclechain.aos.service;

import com.oraclechain.aos.entity.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @since 2020-07-14
 */
public interface ISysRoleService extends IService<SysRole> {

}

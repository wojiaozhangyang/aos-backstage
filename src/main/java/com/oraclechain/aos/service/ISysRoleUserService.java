package com.oraclechain.aos.service;

import com.oraclechain.aos.entity.SysRoleUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户角色中间表 服务类
 * </p>
 *
 * @since 2020-07-14
 */
public interface ISysRoleUserService extends IService<SysRoleUser> {

}

package com.oraclechain.aos.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.oraclechain.aos.entity.FileInfo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @since 2020-07-20
 */
public interface IFileInfoService extends IService<FileInfo> {

}

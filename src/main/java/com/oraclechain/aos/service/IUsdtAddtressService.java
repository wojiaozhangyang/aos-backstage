package com.oraclechain.aos.service;

import com.oraclechain.aos.entity.UsdtAddtress;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @since 2020-08-03
 */
public interface IUsdtAddtressService extends IService<UsdtAddtress> {

}

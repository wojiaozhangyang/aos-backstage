package com.oraclechain.aos.service;

import com.oraclechain.aos.entity.RebateRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 返佣记录
 服务类
 * </p>
 *
 * @since 2020-07-22
 */
public interface IRebateRecordService extends IService<RebateRecord> {

    void toAccount();
}

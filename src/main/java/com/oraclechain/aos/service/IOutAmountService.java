package com.oraclechain.aos.service;

import com.oraclechain.aos.entity.OutAmount;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @since 2020-07-23
 */
public interface IOutAmountService extends IService<OutAmount> {
    Boolean usdtOutAmount(OutAmount outAmount);

    void putForwardTheAccount();

    Boolean aosPutMoney(OutAmount outAmount);

    void checkAosStatus();

    void checkUsdtStatus();
}

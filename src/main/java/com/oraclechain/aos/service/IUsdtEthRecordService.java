
package com.oraclechain.aos.service;

import com.oraclechain.aos.entity.UsdtEthRecord;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @since 2020-07-19
 */
public interface IUsdtEthRecordService extends IService<UsdtEthRecord> {
    void SelectUsdtRecord();


    //获取交易状态是否成功
    String SelectUsdtRecordStatus(String walletAddress);

    List getUsdtToAosRecords();

    String ethBlockNumber();

    //获取Gas
    String getGasOracle();

   //根据哈希获取交易区块
   String ethGetTransactionByHash(String hash);

    void addUsdtRecord() ;
}

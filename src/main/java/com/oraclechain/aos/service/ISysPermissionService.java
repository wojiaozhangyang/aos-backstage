package com.oraclechain.aos.service;

import com.oraclechain.aos.entity.SysPermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 权限表 服务类
 * </p>
 *
 * @since 2020-07-14
 */
public interface ISysPermissionService extends IService<SysPermission> {

    int deleteByPrimaryKey(String ids);
}

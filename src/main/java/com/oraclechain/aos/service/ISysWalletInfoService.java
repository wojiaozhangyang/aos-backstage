package com.oraclechain.aos.service;

import com.oraclechain.aos.entity.SysWalletInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @since 2020-08-06
 */
public interface ISysWalletInfoService extends IService<SysWalletInfo> {

}

package com.oraclechain.aos.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.oraclechain.aos.entity.SysUser;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @since 2020-07-14
 */
public interface ISysUserService extends IService<SysUser> {

}

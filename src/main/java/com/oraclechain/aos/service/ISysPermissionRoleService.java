package com.oraclechain.aos.service;

import com.oraclechain.aos.entity.SysPermissionRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色权限中间表 服务类
 * </p>
 *
 * @since 2020-07-14
 */
public interface ISysPermissionRoleService extends IService<SysPermissionRole> {

}

package com.oraclechain.aos.service;

import com.oraclechain.aos.entity.ShareRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 分享记录 服务类
 * </p>
 *
 * @since 2020-07-17
 */
public interface IShareRecordService extends IService<ShareRecord> {

}

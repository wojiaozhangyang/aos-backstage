package com.oraclechain.aos.service;

import com.oraclechain.aos.entity.AmountRecord;
import com.baomidou.mybatisplus.extension.service.IService;
import com.oraclechain.aos.entity.OutAmount;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @since 2020-07-21
 */
public interface IAmountRecordService extends IService<AmountRecord> {

    /**
     * @Description //账户相关操作记录
     * @Date  2020/7/23
     * @Param
     * @return
     **/

    void saveUsdtAmountRecord();

    void usdtAmountScan();


}

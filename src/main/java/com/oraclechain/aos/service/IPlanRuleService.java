package com.oraclechain.aos.service;

import com.oraclechain.aos.entity.PlanRule;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @since 2020-07-23
 */
public interface IPlanRuleService extends IService<PlanRule> {

}

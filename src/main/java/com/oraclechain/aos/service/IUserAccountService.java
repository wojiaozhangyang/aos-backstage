package com.oraclechain.aos.service;

import com.oraclechain.aos.entity.UserAccount;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户账户 服务类
 * </p>
 *
 * @since 2020-07-16
 */
public interface IUserAccountService extends IService<UserAccount> {

}

package com.oraclechain.aos.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.oraclechain.aos.entity.SysUser;
import com.oraclechain.aos.mapper.SysUserMapper;
import com.oraclechain.aos.service.ISysUserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @since 2020-07-14
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {

}

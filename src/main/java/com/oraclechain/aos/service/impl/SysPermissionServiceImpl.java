package com.oraclechain.aos.service.impl;

import com.oraclechain.aos.entity.SysPermission;
import com.oraclechain.aos.mapper.SysPermissionMapper;
import com.oraclechain.aos.service.ISysPermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.oraclechain.aos.utils.ConvertUtil;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 权限表 服务实现类
 * </p>
 *
 * @since 2020-07-14
 */
@Service
public class SysPermissionServiceImpl extends ServiceImpl<SysPermissionMapper, SysPermission> implements ISysPermissionService {


    @Override
    public int deleteByPrimaryKey(String ids) {
        return 0;
    }
}

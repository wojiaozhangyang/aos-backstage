package com.oraclechain.aos.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.oraclechain.aos.entity.RebateRecord;
import com.oraclechain.aos.entity.UserAccount;
import com.oraclechain.aos.mapper.RebateRecordMapper;
import com.oraclechain.aos.service.IRebateRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.oraclechain.aos.service.IUserAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 返佣记录
 服务实现类
 * </p>
 *
 * @since 2020-07-22
 */
@Service
public class RebateRecordServiceImpl extends ServiceImpl<RebateRecordMapper, RebateRecord> implements IRebateRecordService {

    @Autowired
    private IUserAccountService iUserAccountService;

    @Override
    public void toAccount() {
        List<RebateRecord> rebateRecords = baseMapper.selectList(new QueryWrapper<RebateRecord>().eq("is_release", 2));
        rebateRecords.stream().forEach(rr ->{
            //---------------------------------------------------------------------------------------
            // 设置邀请人账户金额
            UserAccount userAccountTo =
                    iUserAccountService.getOne(
                            new QueryWrapper<UserAccount>().eq("user_id", rr.getToUserId()));
            userAccountTo.setAosBalance(new BigDecimal(rr.getAmount()).add(new BigDecimal(userAccountTo.getAosBalance())).longValue());
             iUserAccountService.updateById(userAccountTo);
             rr.setIsRelease(3);
             baseMapper.updateById(rr);

        });

    }
}

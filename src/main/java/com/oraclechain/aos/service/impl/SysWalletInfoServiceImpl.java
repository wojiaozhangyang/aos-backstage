package com.oraclechain.aos.service.impl;

import com.oraclechain.aos.entity.SysWalletInfo;
import com.oraclechain.aos.mapper.SysWalletInfoMapper;
import com.oraclechain.aos.service.ISysWalletInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @since 2020-08-06
 */
@Service
public class SysWalletInfoServiceImpl extends ServiceImpl<SysWalletInfoMapper, SysWalletInfo> implements ISysWalletInfoService {

}

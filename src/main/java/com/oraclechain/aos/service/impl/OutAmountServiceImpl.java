package com.oraclechain.aos.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.gson.Gson;
import com.oraclechain.aos.blockchain.PushDatamanger;
import com.oraclechain.aos.blockchain.bean.PushSuccessBean;
import com.oraclechain.aos.common.constant.GlobeConstant;
import com.oraclechain.aos.entity.*;
import com.oraclechain.aos.entity.aos.TransferEosMessageBean;
import com.oraclechain.aos.mapper.OutAmountMapper;
import com.oraclechain.aos.service.IOutAmountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.oraclechain.aos.service.ISysWalletInfoService;
import com.oraclechain.aos.service.IUsdtEthRecordService;
import com.oraclechain.aos.service.IUserAccountService;
import com.oraclechain.aos.utils.AosUtils;
import com.oraclechain.aos.utils.ColdWallet;
import com.oraclechain.aos.utils.EOSTransferUtils;
import net.sf.jsqlparser.expression.LongValue;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

/**
 * 服务实现类
 *
 * @since 2020-07-23
 */
@Service
public class OutAmountServiceImpl extends ServiceImpl<OutAmountMapper, OutAmount>
    implements IOutAmountService {

  @Autowired private IUserAccountService iUserAccountService;
  @Autowired private IUsdtEthRecordService iUsdtEthRecordService;
@Autowired private ISysWalletInfoService iSysWalletInfoService;

  @Override
  @Transactional(rollbackFor = Exception.class)
  public Boolean usdtOutAmount(OutAmount outAmount) {
    // 获取当前用户信息
    SysUser principal = (SysUser) SecurityUtils.getSubject().getPrincipal();

    SysWalletInfo sysWalletInfo = iSysWalletInfoService.getById(GlobeConstant.SYS_WALLET_INFO_ID);

    // 提币信息
    outAmount.setUserId(principal.getId());
    outAmount.setStatus(0);
    outAmount.setType(2);
    outAmount.setAudit(0);
    outAmount.setUserEmail(principal.getEmail());
    // 平台账户地址
    outAmount.setFromAddrrss(sysWalletInfo.getSysWalletUsdtAddress());
    int insert = baseMapper.insert(outAmount);

    return insert == 1 ? true : false;
  }

  @Override
  public void putForwardTheAccount() {
    String gasOracle = iUsdtEthRecordService.getGasOracle();

    List<OutAmount> outAmounts =
        baseMapper.selectList(
            new QueryWrapper<OutAmount>().eq("type", 2).eq("status", 0).eq("audit", 1));
    SysWalletInfo sysWalletInfo = iSysWalletInfoService.getById(GlobeConstant.SYS_WALLET_INFO_ID);
    outAmounts.stream()
        .forEach(
            oa -> {
              // 去转账
              String transaction =
                  ColdWallet.transaction(
                          sysWalletInfo.getSysWalletUsdtAddress(),
                      oa.getToAddress(),
                      new BigDecimal(oa.getAmount())
                          .subtract(new BigDecimal(oa.getProcedureFee()))
                          .longValue(),
                      BigInteger.valueOf(Long.parseLong(gasOracle)),
                          sysWalletInfo.getSysWalletUsdtPrivatekey());
              if (transaction != null) {
                oa.setStatus(3);
                oa.setTransactionHash(transaction);
                baseMapper.updateById(oa);
              } else {
                oa.setStatus(2);
                baseMapper.updateById(oa);
              }
            });

    List<OutAmount> aosOutAmounts =
        baseMapper.selectList(
            new QueryWrapper<OutAmount>()
                .eq("type", 1)
                .eq("status", 0)
                .eq("audit", 1)
                .isNull("transaction_hash"));
    aosOutAmounts.stream()
        .forEach(
            aoa -> {
              double x = 1.0 * aoa.getAmount() / GlobeConstant.AOSDECIMAL;
              String amount = String.format("%.4f", x) + " AOS";
              // 去转账
              try {
                new PushDatamanger(
                        new PushDatamanger.Callback() {
                          @Override
                          public void getResult(PushSuccessBean.DataBeanX pushSuccessBean) {
                            if (pushSuccessBean != null) {
                              aoa.setTransactionHash(pushSuccessBean.getTransaction_id());
                              aoa.setStatus(0);
                              baseMapper.updateById(aoa);
                            }
                          }
                        })
                    .pushAction(
                        "aosio.token",
                        "transfer",
                        new Gson()
                            .toJson(
                                new TransferEosMessageBean(
                                    "", aoa.getToAddress(), amount, aoa.getFromAddrrss())),
                        aoa.getFromAddrrss());
              } catch (Exception e) {
                e.printStackTrace();
              }
            });
  }

  @Override
  public Boolean aosPutMoney(OutAmount outAmount) {
    // 获取当前用户信息
    SysUser principal = (SysUser) SecurityUtils.getSubject().getPrincipal();
    SysWalletInfo sysWalletInfo = iSysWalletInfoService.getById(GlobeConstant.SYS_WALLET_INFO_ID);
    // 提币信息
    outAmount.setUserId(principal.getId());
    outAmount.setStatus(0);
    outAmount.setType(1);
    outAmount.setAudit(0);
    outAmount.setUserEmail(principal.getEmail());
    // 平台账户地址
    outAmount.setFromAddrrss(sysWalletInfo.getSysWallAosAddress());
    int insert = baseMapper.insert(outAmount);
    if (insert == 1) {
      // 减去账户钱
      OutAmount byId = baseMapper.selectById(outAmount.getId());
      UserAccount userAccount =
          iUserAccountService.getOne(
              new QueryWrapper<UserAccount>().eq("user_id", byId.getUserId()));
      long amount =
          new BigDecimal(userAccount.getAosBalance())
              .subtract(new BigDecimal(byId.getAmount()))
              .longValue();
      userAccount.setAosBalance(amount);
      iUserAccountService.updateById(userAccount);
    }
    return insert == 1 ? true : false;
  }

  @Override
  public void checkAosStatus() {
    List<OutAmount> outAmounts =
        baseMapper.selectList(
            new QueryWrapper<OutAmount>()
                .eq("audit", 1)
                .eq("status", 0)
                .eq("type", 1)
                .isNotNull("transaction_hash"));
    for (int i = 0; i < outAmounts.size(); i++) {
      Boolean transaction = AosUtils.getTransaction(outAmounts.get(i).getTransactionHash());
      if (transaction) {
        outAmounts.get(i).setStatus(1);
        baseMapper.updateById(outAmounts.get(i));
      }
    }
  }

  @Override
  public void checkUsdtStatus() {
    List<OutAmount> outAmounts =
        baseMapper.selectList(
            new QueryWrapper<OutAmount>()
                .eq("audit", 1)
                .eq("status", 0)
                .or()
                .eq("status", 3)
                .eq("type", 2).isNotNull("transaction_hash"));

    // 获取当前区块高度
    String blockNumber = iUsdtEthRecordService.ethBlockNumber();
    Long blockNum = hexConversionLong(blockNumber);
    if (blockNum != null) {
      for (int i = 0; i < outAmounts.size(); i++) {
        // 根据hash获取交易所在区块高度
        String currentTransactionBlock =
            iUsdtEthRecordService.ethGetTransactionByHash(outAmounts.get(i).getTransactionHash());
        Long currentTransactionBlockNumber = hexConversionLong(currentTransactionBlock);
        // 判断是否可逆
        if (Long.valueOf(currentTransactionBlockNumber) < Long.valueOf(blockNum) - 24) {
          String status =
              iUsdtEthRecordService.SelectUsdtRecordStatus(outAmounts.get(i).getTransactionHash());
          if (Integer.parseInt(status) == 1) {
            OutAmount outAmount = baseMapper.selectById(outAmounts.get(i).getId());
            outAmount.setStatus(1);
            baseMapper.updateById(outAmount);
          }
          if (Integer.parseInt(status) == 0) {
            // 交易失败 返回账户钱数
            OutAmount outAmount = baseMapper.selectById(outAmounts.get(i).getId());
            outAmount.setStatus(2);
            int updateStatus = baseMapper.updateById(outAmount);
            if (updateStatus == 1) {
              // 账户钱
              UserAccount userAccount =
                  iUserAccountService.getOne(
                      new QueryWrapper<UserAccount>().eq("user_id", outAmount.getUserId()));

              // 需要返还的钱数=账户余额加上 （输入数量减去手续费）
              long usdtBalance =
                  new BigDecimal(userAccount.getUsdtBalance())
                      .add(new BigDecimal(outAmount.getAmount()))
                      .multiply(new BigDecimal(outAmount.getProcedureFee()))
                      .longValue();
              userAccount.setUsdtBalance(usdtBalance);
              iUserAccountService.updateById(userAccount);
            }
          }
        }
      }
    }
  }

  private static Long hexConversionLong(String num) {
    if (num.startsWith("0x")) {
      num = num.substring(2);
    }
    Long currentNumber = Long.valueOf(Long.parseLong(num, 16));
    return currentNumber;
  }
}

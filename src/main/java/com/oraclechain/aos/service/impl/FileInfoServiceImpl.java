package com.oraclechain.aos.service.impl;

import com.oraclechain.aos.entity.FileInfo;
import com.oraclechain.aos.mapper.FileInfoMapper;
import com.oraclechain.aos.service.IFileInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @since 2020-07-20
 */
@Service
public class FileInfoServiceImpl extends ServiceImpl<FileInfoMapper, FileInfo> implements IFileInfoService {

}

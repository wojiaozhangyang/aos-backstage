package com.oraclechain.aos.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ejlchina.okhttps.HTTP;
import com.ejlchina.okhttps.HttpResult;
import com.ejlchina.okhttps.HttpTask;
import com.google.gson.Gson;
import com.oraclechain.aos.common.constant.GlobeConstant;
import com.oraclechain.aos.entity.UsdtEthRecord;
import com.oraclechain.aos.entity.param.Record.UsdtToAosRecordParam;
import com.oraclechain.aos.entity.param.usdt.TxReceiptStatusVo;
import com.oraclechain.aos.entity.param.usdt.UsdtEthRecordPage;
import com.oraclechain.aos.entity.usdt.EthGetTransactionByHashDto;
import com.oraclechain.aos.entity.usdt.EthGetTransactionByHashParam;
import com.oraclechain.aos.entity.usdt.GasDto;
import com.oraclechain.aos.entity.usdt.GasResultDto;
import com.oraclechain.aos.mapper.UsdtEthRecordMapper;
import com.oraclechain.aos.service.IUsdtEthRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import okhttp3.ConnectionPool;
import okhttp3.OkHttpClient;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 服务实现类
 *
 * @since 2020-07-19
 */
@Slf4j
@Service
public class UsdtEthRecordServiceImpl extends ServiceImpl<UsdtEthRecordMapper, UsdtEthRecord>
    implements IUsdtEthRecordService {

  private volatile ReentrantLock lock = new ReentrantLock();
  HTTP http =
      HTTP.builder()
          .config(
              (OkHttpClient.Builder builder) -> {
                // 配置连接池 最小10个连接（不配置默认为 5）
                builder.connectionPool(new ConnectionPool(30, 5, TimeUnit.MINUTES));
                // 配置连接超时时间（默认10秒）
                builder.connectTimeout(30, TimeUnit.SECONDS);
                builder.callTimeout(30,TimeUnit.SECONDS);
                builder.writeTimeout(30, TimeUnit.SECONDS);
                builder.readTimeout(30, TimeUnit.SECONDS);
                builder.retryOnConnectionFailure(true);
                builder.pingInterval(60, TimeUnit.SECONDS);
              })
              .exceptionListener((HttpTask<?> task, IOException error) -> {
                task.nothrow();
                // 所有请求执行完都会走这里
                // 返回 true 表示继续执行 task 的 OnComplete 回调，
                // 返回 false 则表示不再执行，即 阻断
                return true;
              })
          .build();



  //https://api.etherscan.io/api?module=account&action=tokentx&contractaddress=0xdac17f958d2ee523a2206206994597c13d831ec7&startblock=10733814&endblock=10733816&sort=asc&apikey=NM1NHX4EJ3TFZKRDHAZDBCV9FKYNGREZA7
  @Override
  public void SelectUsdtRecord(){
        UsdtEthRecord usdtEthRecordOne = baseMapper.selectOne(new QueryWrapper<UsdtEthRecord>().orderByDesc("block_number").last("limit 1"));
        if (usdtEthRecordOne!=null){
            Integer startblockNumber = Integer.valueOf(String.valueOf(new BigDecimal(usdtEthRecordOne.getBlockNumber()).subtract(BigDecimal.valueOf(1))));
            Integer endblockNumber = Integer.valueOf(String.valueOf(new BigDecimal(usdtEthRecordOne.getBlockNumber()).add(BigDecimal.valueOf(10))));
            getRecord(startblockNumber,endblockNumber);
        }else {
            getRecord(10722126,10722136);
        }
    }


    public void getRecord(Integer startblockNumber, Integer endblockNumber) {
        http.async(GlobeConstant.ETH_INTERFACE_ADDRESS)
                .addUrlPara("contractaddress", GlobeConstant.CONTRACT_ADDRESS)
                .addUrlPara("startblock",startblockNumber)
                .addUrlPara("endblock", endblockNumber)
                .addUrlPara("apikey", GlobeConstant.EasyApiKay)
                .addUrlPara("sort", "desc")
                .addUrlPara("action", "tokentx")
                .addUrlPara("module", "account")
                .setOnResponse((HttpResult result) -> {
                    Gson gson = new Gson();
                    String res = result.getBody().toString();
                    UsdtEthRecordPage usdtEthRecordPage =
                            gson.fromJson(res, UsdtEthRecordPage.class);
                    List<UsdtEthRecord> usdtEthRecordList = usdtEthRecordPage.getResult();
                    // 插入数据库
                    if (usdtEthRecordList != null) {
                        for (int i = 0; i < usdtEthRecordList.size(); i++) {
                            UsdtEthRecord hash =
                                    baseMapper.selectOne(
                                            new QueryWrapper<UsdtEthRecord>()
                                                    .select("hash")
                                                    .eq("hash", usdtEthRecordList.get(i).getHash()));
                            if (hash == null) {
                                lock.lock();
                                try {
                                    UsdtEthRecord usdtEthRecord =
                                            baseMapper.selectOne(
                                                    new QueryWrapper<UsdtEthRecord>()
                                                            .select("hash")
                                                            .eq("hash", usdtEthRecordList.get(i).getHash()));
                                    if (usdtEthRecord == null) {
                                        usdtEthRecordList.get(i).setStatus(0);
                                        usdtEthRecordList.get(i).setIsAmountRecord(0);
                                        baseMapper.insert(usdtEthRecordList.get(i));
                                    }
                                } catch (Exception e) {
                                } finally {
                                    lock.unlock();
                                }
                            }
                        }
                    }
                })
                .setOnException((IOException e) -> {
            // 这里处理请求异常
            })
                .get();
    }
//
//    @Override
//  public void SelectUsdtRecord(){
//    // 获取账户交易列表
//
//            http.async(GlobeConstant.ETH_INTERFACE_ADDRESS)
//                    .addUrlPara("contractaddress", GlobeConstant.CONTRACT_ADDRESS)
//                    .addUrlPara("page", "1")
//                    .addUrlPara("offset", "2000")
//                    .addUrlPara("apikey", GlobeConstant.EasyApiKay)
//                    .addUrlPara("sort", "desc")
//                    .addUrlPara("action", "tokentx")
//                    .addUrlPara("module", "account")
//                    .setOnResponse((HttpResult result) -> {
//                      Gson gson = new Gson();
//                        String res = result.getBody().toString();
//                        UsdtEthRecordPage usdtEthRecordPage =
//                              gson.fromJson(res, UsdtEthRecordPage.class);
//                      List<UsdtEthRecord> usdtEthRecordList = usdtEthRecordPage.getResult();
//
//                      // 插入数据库
//                      if (usdtEthRecordList != null) {
//
//                        for (int i = 0; i < usdtEthRecordList.size(); i++) {
//                          UsdtEthRecord hash =
//                                  baseMapper.selectOne(
//                                          new QueryWrapper<UsdtEthRecord>()
//                                                  .select("hash")
//                                                  .eq("hash", usdtEthRecordList.get(i).getHash()));
//                          if (hash == null) {
//                            lock.lock();
//                            try {
//                              UsdtEthRecord usdtEthRecord =
//                                      baseMapper.selectOne(
//                                              new QueryWrapper<UsdtEthRecord>()
//                                                      .select("hash")
//                                                      .eq("hash", usdtEthRecordList.get(i).getHash()));
//                              if (usdtEthRecord == null) {
//                                usdtEthRecordList.get(i).setStatus(0);
//                                usdtEthRecordList.get(i).setIsAmountRecord(0);
//                                baseMapper.insert(usdtEthRecordList.get(i));
//                              }
//                            } catch (Exception e) {
//                            } finally {
//                              lock.unlock();
//                            }
//                          }else {
//                              return;
//                          }
//                        }
//                      }
//                    })
//                    .setOnException((IOException e) -> {
//                      // 这里处理请求异常
//                    })
//                    .get();
//
//
//  }

  @Override
  public String SelectUsdtRecordStatus(String hash) {
    HttpResult result =
        http.async(GlobeConstant.ETH_INTERFACE_ADDRESS)
            .addUrlPara("txhash", hash)
            .addUrlPara("apikey", GlobeConstant.EasyApiKay)
            .addUrlPara("action", "gettxreceiptstatus")
            .addUrlPara("module", "transaction")
            .get()
            .getResult();

    if (result.getBody() != null) {
      // 解析
      TxReceiptStatusVo txReceiptStatusVo =
          JSONObject.parseObject(result.getBody().toString(), TxReceiptStatusVo.class);
      return txReceiptStatusVo.getResult().getStatus();
    }
    return "0";
  }

  @Override
  public List getUsdtToAosRecords() {
    // 获取昨日收盘价
    HttpResult result =
        http.async("http://eth.easy-staking.com/open/api/get_records?symbol=aosusdt&period=1440")
            .get()
            .getResult();

    if (result.getBody() != null) {
      // 解析
      UsdtToAosRecordParam usdtToAosRecordParam =
          JSONObject.parseObject(result.getBody().toString(), UsdtToAosRecordParam.class);

      List list =
          (List) usdtToAosRecordParam.getData().get(usdtToAosRecordParam.getData().size() - 1);
      return list;
    } else {
      getUsdtToAosRecords();
    }
    return null;
  }

  // 获得块高度
  @Override
  public String ethBlockNumber() {
    // 根據hash查詢
    HttpResult result =
        http.async(GlobeConstant.ETH_INTERFACE_ADDRESS)
            .addUrlPara("module", "proxy")
            .addUrlPara("action", "eth_blockNumber")
            .addUrlPara("apikey", GlobeConstant.EasyApiKay)
            .get()
            .getResult();
    Map<String, Object> map;
    map = (Map<String, Object>) JSONObject.parse(result.getBody().toString());
    String blockNum = (String) map.get("result");

    return blockNum;
  }

  @Override
  public String getGasOracle() {
    // 获取账户交易列表
    HttpResult result =
        http.async(GlobeConstant.ETH_INTERFACE_ADDRESS)
            .addUrlPara("apikey", GlobeConstant.EasyApiKay)
            .addUrlPara("sort", "desc")
            .addUrlPara("action", "gasoracle")
            .addUrlPara("module", "gastracker")
            .setOnResponse(
                (HttpResult resulte) -> {
                  // 当发生异常时就不会走这里

                })
            .setOnException(
                (IOException e) -> {
                  // 这里处理请求异常
                })
            .get()
            .getResult();

    Gson gson = new Gson();
    GasResultDto gasResultDto = gson.fromJson(result.getBody().toString(), GasResultDto.class);
    GasDto gasDto = gasResultDto.getResult();
    return gasDto.getSafeGasPrice();
  }

  @Override
  public String ethGetTransactionByHash(String hash) {
    // 获取账户交易列表
    HttpResult result =
            http.async(GlobeConstant.ETH_INTERFACE_ADDRESS)
                    .addUrlPara("apikey", GlobeConstant.EasyApiKay)
                    .addUrlPara("txhash", hash)
                    .addUrlPara("action", "eth_getTransactionByHash")
                    .addUrlPara("module", "proxy")
                    .get()
                    .getResult();

    Gson gson = new Gson();
    EthGetTransactionByHashDto ethGetTransactionByHashDto = gson.fromJson(result.getBody().toString(), EthGetTransactionByHashDto.class);
    EthGetTransactionByHashParam ethGetTransactionByHashParam = ethGetTransactionByHashDto.getResult();
    return ethGetTransactionByHashParam.getBlockNumber();
  }


    @Override
    public void addUsdtRecord() {
        Integer startBlockNumber =10733800;
        Integer endBlockNumber=10741182;
        log.info("-----------------------------》区块记录查询执行开始");
       for (int i=startBlockNumber;i< endBlockNumber;){
            getRecord(startBlockNumber,startBlockNumber+20);
           startBlockNumber = startBlockNumber+20;
           i=startBlockNumber;
           try {
               Thread.sleep(3000);
           } catch (InterruptedException e) {

           }
       }
        log.info("-----------------------------》区块记录查询执行结束");
    }

}

package com.oraclechain.aos.service.impl;

import com.oraclechain.aos.entity.UserAccount;
import com.oraclechain.aos.mapper.UserAccountMapper;
import com.oraclechain.aos.service.IUserAccountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户账户 服务实现类
 * </p>
 *
 * @since 2020-07-16
 */
@Service
public class UserAccountServiceImpl extends ServiceImpl<UserAccountMapper, UserAccount> implements IUserAccountService {

}

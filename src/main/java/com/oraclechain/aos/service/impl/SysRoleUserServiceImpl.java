package com.oraclechain.aos.service.impl;

import com.oraclechain.aos.entity.SysRoleUser;
import com.oraclechain.aos.mapper.SysRoleUserMapper;
import com.oraclechain.aos.service.ISysRoleUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色中间表 服务实现类
 * </p>
 *
 * @since 2020-07-14
 */
@Service
public class SysRoleUserServiceImpl extends ServiceImpl<SysRoleUserMapper, SysRoleUser> implements ISysRoleUserService {

}

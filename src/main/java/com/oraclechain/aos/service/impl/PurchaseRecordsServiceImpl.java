package com.oraclechain.aos.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.oraclechain.aos.common.constant.GlobeConstant;
import com.oraclechain.aos.common.model.Result;
import com.oraclechain.aos.common.wrapper.LanguageValue;
import com.oraclechain.aos.entity.*;
import com.oraclechain.aos.entity.param.Record.AddPurchaseVo;
import com.oraclechain.aos.mapper.PurchaseRecordsMapper;
import com.oraclechain.aos.service.*;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.oraclechain.aos.utils.BeanUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * 申购记录 服务实现类
 *
 * @since 2020-07-16
 */
@Service
public class PurchaseRecordsServiceImpl extends ServiceImpl<PurchaseRecordsMapper, PurchaseRecords>
    implements IPurchaseRecordsService {

  @Autowired private IUserAccountService iUserAccountService;
  @Autowired private ISysRoleUserService iSysRoleUserService;
  @Autowired private ISysUserService iSysUserService;
  @Autowired private IRebateRecordService iRebateRecordService;
  @Autowired private IPlanRuleService iPlanRuleService;
  @Autowired private IUsdtEthRecordService iUsdtEthRecordService;

  // 申购记录保存
  @Override
  @Transactional(rollbackFor = Exception.class)
  public Result addPurchase(String language, AddPurchaseVo addPurchaseVo) {
    //         1存储申购记录
    //         2申购后减去账户usdt余额
    //         3加上aos金额
    SysUser principal = (SysUser) SecurityUtils.getSubject().getPrincipal();

    // 设置账户金额
    UserAccount userAccount =
        iUserAccountService.getOne(
            new QueryWrapper<UserAccount>().eq("user_id", principal.getId()));

     if (addPurchaseVo.getUsdtAmountCosts() == null) {
     if (language.equals(GlobeConstant.CHINESE)){ return Result.fail(GlobeConstant.PleaseEnterTheSubscriptionAmount_Chinese);}
      if (language.equals(GlobeConstant.ENGLISH)){ return Result.fail(GlobeConstant.PleaseEnterTheSubscriptionAmount_English);}
      if (language.equals(GlobeConstant.KOREAN)){ return Result.fail(GlobeConstant.PleaseEnterTheSubscriptionAmount_Korean);}
    }
    if (addPurchaseVo.getUsdtAmountCosts() > userAccount.getUsdtBalance()) {
      if (language.equals(GlobeConstant.CHINESE)){ return Result.fail(GlobeConstant.TheSubscriptionAmountIsLargerThanAccountAmount_Chinese);}
      if (language.equals(GlobeConstant.ENGLISH)){ return Result.fail(GlobeConstant.TheSubscriptionAmountIsLargerThanAccountAmount_English);}
      if (language.equals(GlobeConstant.KOREAN)){ return Result.fail(GlobeConstant.TheSubscriptionAmountIsLargerThanAccountAmount_Korean);}
    }
    if (addPurchaseVo.getUsdtAmountCosts() < (100 * GlobeConstant.USDTDECIMAL)) {
      if (language.equals(GlobeConstant.CHINESE)){ return Result.fail(GlobeConstant.TheMinimumSubscriptionAmountIs_Chinese);}
      if (language.equals(GlobeConstant.ENGLISH)){ return Result.fail(GlobeConstant.TheMinimumSubscriptionAmountIs_English);}
      if (language.equals(GlobeConstant.KOREAN)){ return Result.fail(GlobeConstant.TheMinimumSubscriptionAmountIs_Korean);}
    }

    Long remainingAmount = remainingAmount();
    if (addPurchaseVo.getUsdtAmountCosts() > remainingAmount) {
      if (language.equals(GlobeConstant.CHINESE)){ return Result.fail(GlobeConstant.TheSubscribedAmountIsGreaterThanSubscribedAmount_Chinese);}
      if (language.equals(GlobeConstant.ENGLISH)){ return Result.fail(GlobeConstant.TheSubscribedAmountIsGreaterThanSubscribedAmount_English);}
      if (language.equals(GlobeConstant.KOREAN)){ return Result.fail(GlobeConstant.TheSubscribedAmountIsGreaterThanSubscribedAmount_Korean);}
    }
    // 获取平台默认规则
    PlanRule planRule = iPlanRuleService.getById(GlobeConstant.IPLANRULEID);
    // 拷贝对象,设置申购信息
    PurchaseRecords purchaseRecord = new PurchaseRecords();
    BeanUtils.copyBeanProp(purchaseRecord, addPurchaseVo);
    purchaseRecord.setAosActualDay(planRule.getReleaseCount());
    purchaseRecord.setExtraPredictDay(planRule.getExtraPredictCount());
    purchaseRecord.setAosActualCount(0);
    purchaseRecord.setExtraPredictCount(0);
    purchaseRecord.setIsAmount(0);
    purchaseRecord.setUserId(principal.getId());
    // 保存数据库
    int insert = baseMapper.insert(purchaseRecord);
    BigDecimal usdtAmount =
        new BigDecimal(userAccount.getUsdtBalance())
            .subtract(new BigDecimal(purchaseRecord.getUsdtAmountCosts()));
    userAccount.setUsdtBalance(usdtAmount.longValue());
    // 设置AOS账户金额
    //获得昨日收盘价
    BigDecimal closePrice = (BigDecimal) iUsdtEthRecordService.getUsdtToAosRecords().get(4);
    // 原账户的AOS金额+ usdt申购的金额*换算单位*0.01
    BigDecimal UsdtAmountAos =
        new BigDecimal(purchaseRecord.getUsdtAmountCosts())
            .divide(closePrice, 4, RoundingMode.DOWN).divide(BigDecimal.valueOf(GlobeConstant.USDTDECIMAL));
    BigDecimal divide = UsdtAmountAos.multiply(new BigDecimal(0.01)).multiply(BigDecimal.valueOf(GlobeConstant.AOSDECIMAL));
    BigDecimal aosCount = new BigDecimal(userAccount.getAosBalance()).add(divide);
    userAccount.setAosBalance(aosCount.longValue());

    //----------记录自己返现的aos
    RebateRecord rebateR = new RebateRecord();
    rebateR.setType(1);
    rebateR.setFromUserId(purchaseRecord.getUserId());
    rebateR.setToUserId(purchaseRecord.getUserId());
    rebateR.setAmount(divide.longValue());
    rebateR.setIsRelease(3);
    rebateR.setStatus(1);
    rebateR.setUserEmail(principal.getEmail());
    rebateR.setUsdtAmountCosts(addPurchaseVo.getUsdtAmountCosts());
    iRebateRecordService.save(rebateR);
    //更新账户
    boolean b = iUserAccountService.updateById(userAccount);
    // 更新当日申购次数
    PurchaseRecords purchas = new PurchaseRecords();
    purchas.setId(purchaseRecord.getId());
    purchas.setAosActualCount(purchaseRecord.getAosActualCount()+1);
    baseMapper.updateById(purchas);
    // 给邀请人返usdt金额 记录交易记录--------------------------------------
    SysUser inviterBy =
        iSysUserService.getOne(
            new QueryWrapper<SysUser>().eq("inviter_by", purchaseRecord.getUserId()));
    Integer istatus = 0;
    if (inviterBy != null) {
      // ---------------------给邀请人返usdt金额-------
      UserAccount userAccountOne =
          iUserAccountService.getOne(
              new QueryWrapper<UserAccount>().eq("user_id", inviterBy.getId()));
      if (userAccountOne != null) {
        long allPrice =
            new BigDecimal(userAccountOne.getUsdtBalance())
                .add(
                    new BigDecimal(purchaseRecord.getUsdtAmountCosts())
                        .multiply(new BigDecimal("0.05")))
                .longValue();
        userAccountOne.setUsdtBalance(allPrice);
        iUserAccountService.updateById(userAccountOne);
    //----------记录直推返佣
        RebateRecord rebate = new RebateRecord();
        rebate.setType(2);
        rebate.setFromUserId(inviterBy.getId());
        rebate.setToUserId(purchaseRecord.getUserId());
        rebate.setAmount(allPrice);
        rebate.setIsRelease(3);
        rebate.setStatus(1);
        rebate.setUserEmail(principal.getEmail());
        rebate.setUsdtAmountCosts(addPurchaseVo.getUsdtAmountCosts());
        iRebateRecordService.save(rebate);

        // 更新表记录  状态更新
        PurchaseRecords purchaseRecords = new PurchaseRecords();
        purchaseRecords.setId(purchaseRecord.getId());
        purchaseRecords.setIsRebate(1);
        istatus = baseMapper.updateById(purchaseRecords);
      }

      long price =
          new BigDecimal(purchaseRecord.getUsdtAmountCosts())
              .multiply(new BigDecimal("0.05"))
              .longValue();
      RebateRecord rebateRecord = new RebateRecord();
      rebateRecord.setType(3);
      rebateRecord.setFromUserId(inviterBy.getId());
      rebateRecord.setToUserId(purchaseRecord.getUserId());
      rebateRecord.setAmount(price);
      rebateRecord.setIsRelease(3);
      rebateRecord.setStatus(istatus);
      rebateRecord.setCurrentDay(purchaseRecord.getAosActualCount());
      rebateRecord.setTotalDay(purchaseRecord.getAosActualDay());
      rebateRecord.setUserEmail(principal.getEmail());
      rebateRecord.setUsdtAmountCosts(addPurchaseVo.getUsdtAmountCosts());
      iRebateRecordService.save(rebateRecord);
    } else {
      //设置申购记录中未给直推用户返佣状态
      PurchaseRecords purchaseRecords = new PurchaseRecords();
      purchaseRecords.setId(purchaseRecord.getId());
      purchaseRecords.setIsRebate(0);
      baseMapper.updateById(purchaseRecords);
    }
    if (language.equals(GlobeConstant.CHINESE)){ return Result.fail(GlobeConstant.SuccessfulApplications_Chinese);}
    if (language.equals(GlobeConstant.ENGLISH)){ return Result.fail(GlobeConstant.SuccessfulApplications_English);}
    if (language.equals(GlobeConstant.KOREAN)){ return Result.fail(GlobeConstant.SuccessfulApplications_Korean);}
    return Result.ok("申购成功");
  }

  // 一天更新一次
  @Override
  public void TimingCommission() {
    List<PurchaseRecords> purchaseRecords =
        baseMapper.selectList(new QueryWrapper<PurchaseRecords>().eq("is_amount", 1));
    purchaseRecords.stream()
        .forEach(
            pr -> {
              // 获取邀请人信息
              SysUser userInfo = iSysUserService.getById(pr.getUserId());
              if (pr.getAosActualCount() <= pr.getAosActualDay()) {
                // 给自己解锁
                pr.setAosActualCount(pr.getAosActualCount() + 1);
                // 设置AOS账户金额
                BigDecimal closePrice =
                    (BigDecimal) iUsdtEthRecordService.getUsdtToAosRecords().get(4);
                // 原账户的AOS金额+ usdt申购的金额*换算单位*0.1 *換算價格
                BigDecimal divide =
                    new BigDecimal(pr.getUsdtAmountCosts())
                        .divide(closePrice, 4, RoundingMode.DOWN).divide(new BigDecimal(GlobeConstant.AOSDECIMAL))
                        .multiply(new BigDecimal("0.01"));

                // -------------------释放记录--------------------
                RebateRecord rebate = new RebateRecord();
                rebate.setCurrentDay(pr.getAosActualCount());
                rebate.setTotalDay(pr.getAosActualDay());
                rebate.setType(3);
                rebate.setAmount(divide.longValue());
                rebate.setFromUserId(pr.getUserId());
                rebate.setToUserId(pr.getUserId());
                rebate.setUserEmail(userInfo.getEmail());
                rebate.setStatus(0);
                rebate.setIsRelease(1);
                rebate.setUsdtAmountCosts(pr.getUsdtAmountCosts());
                iRebateRecordService.save(rebate);


                // 获取用户身份
                SysRoleUser sysRoleUser =
                    iSysRoleUserService.getOne(
                        new QueryWrapper<SysRoleUser>().eq("sys_user_id", pr.getUserId()));

                // 如果邀请人不为null
                if (userInfo.getInviterBy() != null) {
                  // ------判断用户角色-----------
                  // 判断是否是超级天使，伙伴
                  if (GlobeConstant.PARTNER.equals(sysRoleUser.getSysRoleId())) {
                    // 获取邀请人信息
                    SysUser inviterBy = iSysUserService.getById(userInfo.getInviterBy());

                    // -------------------返佣记录--------------------
                    RebateRecord rebateRecord = new RebateRecord();
                    rebateRecord.setFromUserId(pr.getUserId());
                    rebateRecord.setToUserId(inviterBy.getId());
                    rebateRecord.setAmount(divide.multiply(new BigDecimal(0.02)).longValue());
                    rebateRecord.setStatus(0);
                    rebateRecord.setType(1);
                    rebateRecord.setCurrentDay(pr.getAosActualCount());
                    rebateRecord.setTotalDay(pr.getAosActualDay());
                    rebateRecord.setUsdtAmountCosts(pr.getUsdtAmountCosts());
                    rebateRecord.setUserEmail(inviterBy.getEmail());
                    rebateRecord.setIsRelease(1);
                    rebateRecord.setUsdtAmountCosts(pr.getUsdtAmountCosts());
                    iRebateRecordService.save(rebateRecord);
                  }
                }
              } else {
                // ---返还额外奖励
                if (pr.getExtraPredictCount() < pr.getExtraPredictDay()) {
                  pr.setExtraPredictCount(pr.getExtraPredictCount() + 1);
                  // 设置AOS账户金额
                  BigDecimal closePrice =
                      (BigDecimal) iUsdtEthRecordService.getUsdtToAosRecords().get(4);
                  // 原账户的AOS金额+ usdt申购的金额*换算单位*0.1
                  BigDecimal divide =
                      new BigDecimal(pr.getUsdtAmountCosts())
                          .divide(closePrice, 4, RoundingMode.DOWN)
                          .multiply(new BigDecimal("0.01"));

                  // -------------------释放记录--------------------
                  RebateRecord rebate = new RebateRecord();
                  rebate.setCurrentDay(pr.getAosActualCount());
                  rebate.setTotalDay(pr.getAosActualDay());
                  rebate.setType(3);
                  rebate.setAmount(divide.longValue());
                  rebate.setFromUserId(pr.getUserId());
                  rebate.setToUserId(pr.getUserId());
                  rebate.setUserEmail(userInfo.getEmail());
                  rebate.setStatus(0);
                  rebate.setIsRelease(1);
                  rebate.setUsdtAmountCosts(pr.getUsdtAmountCosts());


//                  UserAccount userAccount =
//                      iUserAccountService.getOne(
//                          new QueryWrapper<UserAccount>().eq("user_id", pr.getUserId()));
//                  BigDecimal aosCount = new BigDecimal(userAccount.getAosBalance()).add(divide);
//                  userAccount.setAosBalance(aosCount.longValue());
//                  iUserAccountService.updateById(userAccount);
                }
              }
            });
  }

  @Override
  public Long remainingAmount() {
    SysUser principal = (SysUser) SecurityUtils.getSubject().getPrincipal();
    List<PurchaseRecords> purchaseRecords = baseMapper.selectList(new QueryWrapper<PurchaseRecords>().eq("user_id",principal.getId()));
    //获取现在已经申购额度
    BigDecimal amount = new BigDecimal(0);

    for (int i=0; i<purchaseRecords.size(); i++){
      amount =  amount.add(new BigDecimal(purchaseRecords.get(i).getUsdtAmountCosts()));
    }
    SysRoleUser sysRoleUser = iSysRoleUserService.getOne(new QueryWrapper<SysRoleUser>().eq("sys_user_id", principal.getId()));
    BigDecimal remainingAmount;
    if (GlobeConstant.PARTNER.equals(sysRoleUser.getSysRoleId())){
      remainingAmount = new BigDecimal(1500*GlobeConstant.USDTDECIMAL).subtract(amount);
    }else {
      remainingAmount = new BigDecimal(7000*GlobeConstant.USDTDECIMAL).subtract(amount);
    }

    return remainingAmount.longValue();
  }


}

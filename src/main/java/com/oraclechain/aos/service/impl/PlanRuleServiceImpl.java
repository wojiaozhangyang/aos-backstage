package com.oraclechain.aos.service.impl;

import com.oraclechain.aos.entity.PlanRule;
import com.oraclechain.aos.mapper.PlanRuleMapper;
import com.oraclechain.aos.service.IPlanRuleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @since 2020-07-23
 */
@Service
public class PlanRuleServiceImpl extends ServiceImpl<PlanRuleMapper, PlanRule> implements IPlanRuleService {

}

package com.oraclechain.aos.service.impl;

import com.oraclechain.aos.entity.UsdtAddtress;
import com.oraclechain.aos.mapper.UsdtAddtressMapper;
import com.oraclechain.aos.service.IUsdtAddtressService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @since 2020-08-03
 */
@Service
public class UsdtAddtressServiceImpl extends ServiceImpl<UsdtAddtressMapper, UsdtAddtress> implements IUsdtAddtressService {

}

package com.oraclechain.aos.service.impl;

import com.oraclechain.aos.entity.SysRole;
import com.oraclechain.aos.mapper.SysRoleMapper;
import com.oraclechain.aos.service.ISysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @since 2020-07-14
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

}

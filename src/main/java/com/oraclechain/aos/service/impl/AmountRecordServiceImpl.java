package com.oraclechain.aos.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.oraclechain.aos.entity.*;
import com.oraclechain.aos.mapper.AmountRecordMapper;
import com.oraclechain.aos.service.*;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import freemarker.ext.beans.TemplateAccessible;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 服务实现类
 *
 * @since 2020-07-21
 */
@Service
public class AmountRecordServiceImpl extends ServiceImpl<AmountRecordMapper, AmountRecord>
    implements IAmountRecordService {

  @Autowired private IUsdtEthRecordService iUsdtEthRecordService;
  @Autowired private IUserAccountService iUserAccountService;
  @Autowired private ISysUserService iSysUserService;
  @Autowired private IUsdtAddtressService iUsdtAddtressService;
  // 根据eth交易列表获取udst账户状态
  @Override
  public void saveUsdtAmountRecord() {

    List<UsdtAddtress> usdtAddtressList =
        iUsdtAddtressService.list(new QueryWrapper<UsdtAddtress>().eq("is_use", 1));
    List<String> list = new ArrayList<>();
    for (int i = 0; i < usdtAddtressList.size(); i++) {
      list.add(usdtAddtressList.get(i).getAddress().toLowerCase());
    }

    List<UsdtEthRecord> usdtEthRecordList =
        iUsdtEthRecordService.list(
            new QueryWrapper<UsdtEthRecord>()
                .eq("is_amount_record", 0)
                .eq("status", 1)
                .in("`to`", list));

    for (int i = 0; i < usdtEthRecordList.size(); i++) {
     String    toAddress =  usdtEthRecordList.get(i).getTo().toLowerCase();
      SysUser sysUser =
          iSysUserService.getOne(new QueryWrapper<SysUser>().eq("lower(wallet_address)",toAddress));
      if (sysUser != null) {
        // 设置转账记录
        AmountRecord amountRecord = new AmountRecord();
        amountRecord.setUserId(sysUser.getId());
        amountRecord.setType(2);
        amountRecord.setUsdtAmount(usdtEthRecordList.get(i).getValue());
        amountRecord.setWalletAddress(toAddress);
        amountRecord.setStatus(0);
        amountRecord.setUsdtEthRecordId(usdtEthRecordList.get(i).getId());
        baseMapper.insert(amountRecord);
        usdtEthRecordList.get(i).setIsAmountRecord(1);
        iUsdtEthRecordService.updateById(usdtEthRecordList.get(i));
        // 设置账户金额
        UserAccount userAccount =
            iUserAccountService.getOne(
                new QueryWrapper<UserAccount>().eq("user_id", sysUser.getId()));
        BigDecimal usdtBalance =
            new BigDecimal(userAccount.getUsdtBalance())
                .add(new BigDecimal(usdtEthRecordList.get(i).getValue()));
        userAccount.setUsdtBalance(usdtBalance.longValue());
        iUserAccountService.updateById(userAccount);
        // 更新账户状态
        AmountRecord amountRecordset = new AmountRecord();
        amountRecordset.setId(amountRecord.getId());
        amountRecordset.setStatus(1);
        baseMapper.updateById(amountRecordset);
      }
    }
  }

  /** @Description //定时扫描账户是否充值成功 USDT充值相关 判断eth账户列表 @Date 2020/7/23 @Param []
   *
   * @return void
   */
  @Override
  public void usdtAmountScan() {
      List<UsdtAddtress> usdtAddtressList =
              iUsdtAddtressService.list(new QueryWrapper<UsdtAddtress>().eq("is_use", 1));
      List<String> list = new ArrayList<>();
      for (int i = 0; i < usdtAddtressList.size(); i++) {
          list.add(usdtAddtressList.get(i).getAddress().toLowerCase());
      }
    // 获取usdteth的交易记录
    List<UsdtEthRecord> usdtEthRecordList =
        iUsdtEthRecordService.list(new QueryWrapper<UsdtEthRecord>().eq("status", 0).in("`to`", list));
    // 遍历hash
    usdtEthRecordList.stream()
        .forEach(
            uerl -> {
              // 获取状态
              String status = iUsdtEthRecordService.SelectUsdtRecordStatus(uerl.getHash());
              uerl.setStatus(Integer.parseInt(status));
              iUsdtEthRecordService.updateById(uerl);
            });
  }
}

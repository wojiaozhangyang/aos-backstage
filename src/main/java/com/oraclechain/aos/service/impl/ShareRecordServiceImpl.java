package com.oraclechain.aos.service.impl;

import com.oraclechain.aos.entity.ShareRecord;
import com.oraclechain.aos.mapper.ShareRecordMapper;
import com.oraclechain.aos.service.IShareRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 分享记录 服务实现类
 * </p>
 *
 * @since 2020-07-17
 */
@Service
public class ShareRecordServiceImpl extends ServiceImpl<ShareRecordMapper, ShareRecord> implements IShareRecordService {



}

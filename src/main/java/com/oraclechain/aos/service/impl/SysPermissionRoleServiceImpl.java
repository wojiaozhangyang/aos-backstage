package com.oraclechain.aos.service.impl;

import com.oraclechain.aos.entity.SysPermissionRole;
import com.oraclechain.aos.mapper.SysPermissionRoleMapper;
import com.oraclechain.aos.service.ISysPermissionRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色权限中间表 服务实现类
 * </p>
 *
 * @since 2020-07-14
 */
@Service
public class SysPermissionRoleServiceImpl extends ServiceImpl<SysPermissionRoleMapper, SysPermissionRole> implements ISysPermissionRoleService {

}

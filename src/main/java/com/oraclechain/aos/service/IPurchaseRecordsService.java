package com.oraclechain.aos.service;

import com.oraclechain.aos.common.model.Result;
import com.oraclechain.aos.entity.PurchaseRecords;
import com.baomidou.mybatisplus.extension.service.IService;
import com.oraclechain.aos.entity.param.Record.AddPurchaseVo;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 申购记录 服务类
 * </p>
 *
 * @since 2020-07-16
 */
public interface IPurchaseRecordsService extends IService<PurchaseRecords> {

    Result addPurchase(String language, AddPurchaseVo addPurchaseVo);

    void TimingCommission();

    Long remainingAmount();

}

package com.oraclechain.aos.blockchain.bean;

import com.google.gson.JsonElement;

/**
 * Created by pocketEos on 2018/9/18.
 */

public class BeanToJsonResultBean {

    /**
     * code : 0
     * message : ok
     * data : {"args":{"quantity":"0.0001 EOS","memo":"memo","from":"weiweikenwo1","to":"sgyxhomesgyx"}}
     */

    private int code;
    private String message;
    private DataBean data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * args : {"quantity":"0.0001 EOS","memo":"memo","from":"weiweikenwo1","to":"sgyxhomesgyx"}
         */

        private JsonElement args;

        public JsonElement getArgs() {
            return args;
        }

        public void setArgs(JsonElement args) {
            this.args = args;
        }
    }
}

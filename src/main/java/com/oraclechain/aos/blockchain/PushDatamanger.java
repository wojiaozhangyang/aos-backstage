package com.oraclechain.aos.blockchain;

import cn.hutool.http.GlobalHeaders;
import cn.hutool.http.HttpRequest;
import com.ejlchina.okhttps.HttpResult;
import com.ejlchina.okhttps.OkHttps;

import com.oraclechain.aos.blockchain.api.EosChainInfo;
import com.oraclechain.aos.blockchain.bean.JsonToBeanResultBean;
import com.oraclechain.aos.blockchain.bean.JsonToBinRequest;
import com.oraclechain.aos.blockchain.bean.PushSuccessBean;
import com.oraclechain.aos.blockchain.chain.Action;
import com.oraclechain.aos.blockchain.chain.PackedTransaction;
import com.oraclechain.aos.blockchain.chain.SignedTransaction;
import com.oraclechain.aos.blockchain.cypto.ec.EosPrivateKey;
import com.oraclechain.aos.blockchain.types.TypeChainId;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;

import com.oraclechain.aos.blockchain.util.JsonUtil;
import com.oraclechain.aos.common.constant.GlobeConstant;
import lombok.extern.slf4j.Slf4j;
import okhttp3.MediaType;

@Slf4j
public class PushDatamanger {
  Callback mCallback;
  EosChainInfo mChainInfoBean = new EosChainInfo();
  JsonToBeanResultBean mJsonToBeanResultBean = new JsonToBeanResultBean();
  String[] permissions;
  SignedTransaction txnBeforeSign;

  String contract, action, message;

  //  public PushDatamanger(String userpassword, Callback callback) {
  //    this.userpassword = userpassword;
  //    mCallback = callback;
  //  }
  public PushDatamanger(Callback callback) {
    mCallback = callback;
  }

  public void pushAction(String contract, String action, String message, String permissionAccount) {
    this.message = message;
    this.contract = contract;
    this.action = action;
    permissions = getActivePermission(permissionAccount);
    getChainInfo();
  }

  public void pushOwnerAction(
      String contract, String action, String message, String permissionAccount) {
    this.message = message;
    this.contract = contract;
    this.action = action;

    permissions = getOwnerPermission(permissionAccount);
    getChainInfo();
  }

  private String[] getActivePermission(String accountName) {
    return new String[] {accountName + "@active"};
  }

  private String[] getOwnerPermission(String accountName) {
    return new String[] {accountName + "@owner"};
  }

  public void getChainInfo() {

    HttpResult result =
        OkHttps.async("http://api.aos.plus:8888/v1/chain/get_info")
            .setOnResponse(
                (HttpResult res) -> {
                  // 响应回调
                  int status = res.getStatus(); // 状态码
                  if (status == 200) {
                    mChainInfoBean =
                        (EosChainInfo)
                            JsonUtil.parseStringToBean(
                                res.getBody().toString(), EosChainInfo.class);
                    getabi_json_to_bin();
                  }
                })
            .setOnException(
                (IOException e) -> {
                  // 异常回调

                })
            .get()
            .getResult();
  }

  public void getabi_json_to_bin() {
    JsonToBinRequest jsonToBinRequest =
        new JsonToBinRequest(contract, action, message.replaceAll("\\r|\\n", ""));

    //链式构建请求
    String result = HttpRequest.post("http://api.aos.plus:8888/v1/chain/abi_json_to_bin")
            .body(new Gson().toJson(jsonToBinRequest))//表单内容
            //超时，毫秒
            .timeout(30000)
            .execute().body();

    mJsonToBeanResultBean =
            (JsonToBeanResultBean)
                    JsonUtil.parseStringToBean(
                           result, JsonToBeanResultBean.class);
    txnBeforeSign =
            createTransaction(
                    contract,
                    action,
                    mJsonToBeanResultBean.getBinargs(),
                    permissions,
                    mChainInfoBean);
    EosPrivateKey eosPrivateKey =
            new EosPrivateKey(GlobeConstant.EosApiKay);
    txnBeforeSign.sign(
            eosPrivateKey, new TypeChainId(mChainInfoBean.getChain_id()));
    pushTransactionRetJson(new PackedTransaction(txnBeforeSign));

  }

  private SignedTransaction createTransaction(
      String contract,
      String actionName,
      String dataAsHex,
      String[] permissions,
      EosChainInfo chainInfo) {

    Action action = new Action(contract, actionName);
    action.setAuthorization(permissions);
    action.setData(dataAsHex);

    SignedTransaction txn = new SignedTransaction();
    txn.addAction(action);
    txn.putSignatures(new ArrayList<String>());

    if (null != chainInfo) {
      txn.setReferenceBlock(chainInfo.getHeadBlockId());
      txn.setExpiration(chainInfo.getTimeAfterHeadBlockTime(30000));
    }
    return txn;
  }


  public void pushTransactionRetJson(PackedTransaction body) {


    //链式构建请求
    String result = HttpRequest.post("http://api.aos.plus:8888/v1/chain/push_transaction")
            .body(new Gson().toJson(body))//表单内容
            //超时，毫秒
            .timeout(30000)
            .execute().body();
    PushSuccessBean.DataBeanX dataBeanX =
            (PushSuccessBean.DataBeanX)
                    JsonUtil.parseStringToBean(
                           result, PushSuccessBean.DataBeanX.class);
    mCallback.getResult(dataBeanX);

  }

  public interface Callback {
    void getResult(PushSuccessBean.DataBeanX pushSuccessBean);
  }

  private static boolean isPlaintext(MediaType mediaType) {
    if (mediaType == null) {
      return false;
    }
    if (mediaType.type() != null && mediaType.type().equals("text")) {
      return true;
    }
    String subtype = mediaType.subtype();
    if (subtype != null) {
      subtype = subtype.toLowerCase();
      if (subtype.contains("x-www-form-urlencoded")
          || subtype.contains("json")
          || subtype.contains("xml")
          || subtype.contains("html")) //
      return true;
    }
    return false;
  }
}

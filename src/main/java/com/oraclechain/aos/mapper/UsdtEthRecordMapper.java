package com.oraclechain.aos.mapper;

import com.oraclechain.aos.entity.UsdtEthRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @since 2020-07-19
 */
public interface UsdtEthRecordMapper extends BaseMapper<UsdtEthRecord> {

}

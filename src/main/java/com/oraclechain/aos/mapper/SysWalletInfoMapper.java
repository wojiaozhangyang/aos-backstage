package com.oraclechain.aos.mapper;

import com.oraclechain.aos.entity.SysWalletInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @since 2020-08-06
 */
public interface SysWalletInfoMapper extends BaseMapper<SysWalletInfo> {

}

package com.oraclechain.aos.mapper;

import com.oraclechain.aos.entity.AmountRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @since 2020-07-21
 */
public interface AmountRecordMapper extends BaseMapper<AmountRecord> {

}

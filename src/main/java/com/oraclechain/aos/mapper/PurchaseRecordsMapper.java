package com.oraclechain.aos.mapper;

import com.oraclechain.aos.entity.PurchaseRecords;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 申购记录 Mapper 接口
 * </p>
 *
 * @since 2020-07-16
 */
public interface PurchaseRecordsMapper extends BaseMapper<PurchaseRecords> {

}

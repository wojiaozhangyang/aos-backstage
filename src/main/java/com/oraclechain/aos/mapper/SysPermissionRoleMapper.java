package com.oraclechain.aos.mapper;

import com.oraclechain.aos.entity.SysPermissionRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色权限中间表 Mapper 接口
 * </p>
 *
 * @since 2020-07-14
 */
public interface SysPermissionRoleMapper extends BaseMapper<SysPermissionRole> {

}

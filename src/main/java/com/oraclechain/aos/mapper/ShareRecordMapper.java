package com.oraclechain.aos.mapper;

import com.oraclechain.aos.entity.ShareRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 分享记录 Mapper 接口
 * </p>
 *
 * @since 2020-07-17
 */
public interface ShareRecordMapper extends BaseMapper<ShareRecord> {

}

package com.oraclechain.aos.mapper;

import com.oraclechain.aos.entity.RebateRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 返佣记录
 Mapper 接口
 * </p>
 *
 * @since 2020-07-22
 */
public interface RebateRecordMapper extends BaseMapper<RebateRecord> {

}

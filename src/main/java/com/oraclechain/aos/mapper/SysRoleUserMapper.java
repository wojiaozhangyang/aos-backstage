package com.oraclechain.aos.mapper;

import com.oraclechain.aos.entity.SysRoleUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户角色中间表 Mapper 接口
 * </p>
 *
 * @since 2020-07-14
 */
public interface SysRoleUserMapper extends BaseMapper<SysRoleUser> {

}

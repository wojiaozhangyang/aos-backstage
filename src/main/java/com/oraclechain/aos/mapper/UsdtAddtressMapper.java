package com.oraclechain.aos.mapper;

import com.oraclechain.aos.entity.UsdtAddtress;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @since 2020-08-03
 */
public interface UsdtAddtressMapper extends BaseMapper<UsdtAddtress> {

}

package com.oraclechain.aos.mapper;

import com.oraclechain.aos.entity.OutAmount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @since 2020-07-23
 */
public interface OutAmountMapper extends BaseMapper<OutAmount> {

}

package com.oraclechain.aos.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.oraclechain.aos.entity.FileInfo;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @since 2020-07-20
 */
public interface FileInfoMapper extends BaseMapper<FileInfo> {

}

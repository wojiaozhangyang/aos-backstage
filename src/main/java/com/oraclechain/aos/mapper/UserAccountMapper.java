package com.oraclechain.aos.mapper;

import com.oraclechain.aos.entity.UserAccount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户账户 Mapper 接口
 * </p>
 *
 * @since 2020-07-16
 */
public interface UserAccountMapper extends BaseMapper<UserAccount> {

}

package com.oraclechain.aos.mapper;

import com.oraclechain.aos.entity.SysPermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 权限表 Mapper 接口
 * </p>
 *
 * @since 2020-07-14
 */
public interface SysPermissionMapper extends BaseMapper<SysPermission> {

    /**
     * 查询全部权限
     * @return
     */
    List<SysPermission> findAll();

    /**
     * 根据用户id查询出用户的所有权限
     * @param userId
     * @return
     */
    List<SysPermission> findByAdminUserId(String userId);

    /**
     * 根据角色id查询权限
     * @param roleid
     * @return
     */
    List<SysPermission> queryRoleId(String roleid);
}

package com.oraclechain.aos.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.oraclechain.aos.entity.SysUser;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @since 2020-07-14
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}

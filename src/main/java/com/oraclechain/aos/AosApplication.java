package com.oraclechain.aos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@ServletComponentScan
@SpringBootApplication
public class AosApplication {

    public static void main(String[] args) {
        SpringApplication.run(AosApplication.class, args);
    }

}

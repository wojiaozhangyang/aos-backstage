package com.oraclechain.aos.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.oraclechain.aos.common.model.SuperEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;


/**
 * <p>
 * 用户表
 * </p>
 *
 * @since 2020-07-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="SysUser对象", description="用户表")
public class SysUser extends SuperEntity implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "用户账号")
    @Email(message = "邮箱格式错误")
    private String email;

    @ApiModelProperty(value = "用户密码")
    private String password;


    @ApiModelProperty(value = "邀请码")
    private String inviteCode;

    @ApiModelProperty(value = "邀请人")
    private String inviterBy;

    @ApiModelProperty(value = "状态 0:未激活 1已激活正常使用")
    private Integer emailStatus;

    @ApiModelProperty(value = "邮箱激活码")
    private String emailCode;

    @ApiModelProperty(value = "盐值")
    private String salt;

    @ApiModelProperty(value = "审核状态")
    private Integer auditStatus;

    @ApiModelProperty(value = "账户地址")
    private String walletAddress;

    @ApiModelProperty(value = "二維碼")
    private String qrCodePath;

    @ApiModelProperty(value = "是否符合升级规则 0不可以 1可以")
    private Integer isUpgrade;
}

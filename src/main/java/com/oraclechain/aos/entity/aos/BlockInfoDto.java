package com.oraclechain.aos.entity.aos;

import lombok.Data;

import java.util.Date;

/**
 * @ClassName: BlockInfo
 * @Description: Todo
 * @data: 2020/7/30  16:09
 */
@Data
public class BlockInfoDto {
    private String server_version;
    private String chain_id;
    private long head_block_num;
    private long last_irreversible_block_num;
    private String last_irreversible_block_id;
    private String head_block_id;
    private Date head_block_time;
    private String head_block_producer;
    private long virtual_block_cpu_limit;
    private long virtual_block_net_limit;
    private long block_cpu_limit;
    private long block_net_limit;
    private String server_version_string;
    private long fork_db_head_block_num;
    private String fork_db_head_block_id;
    private String server_full_version_string;
}
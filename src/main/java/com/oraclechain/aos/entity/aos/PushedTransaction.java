package com.oraclechain.aos.entity.aos;


import lombok.Data;

/**
 * @ClassName: PushedTransaction
 * @Description: Todo
 * @data: 2020/7/30  13:22
 */
@Data
public class PushedTransaction {
    private Processed processed;
    private String transactionId;
}

package com.oraclechain.aos.entity.aos;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @ClassName: BlockVo
 * @Description: Todo
 * @data: 2020/7/30  16:11
 */
@Data
public class BlockDto {
    private String previous;
    private Date timestamp;
    private String transaction_merkle_root;
    private String producer;
    private List<String> producer_changes;
    private String producer_signature;
    private List<String> cycles;
    private String id;
    private int block_num;
    private long ref_block_prefix;
}

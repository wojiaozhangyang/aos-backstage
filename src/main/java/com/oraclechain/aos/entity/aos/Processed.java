package com.oraclechain.aos.entity.aos;

import io.jafka.jeos.core.common.ActionTrace;
import io.jafka.jeos.core.response.chain.transaction.Receipt;
import lombok.Data;

import java.util.List;

/**
 * @ClassName: Processed
 * @Description: Todo
 * @data: 2020/7/30  13:23
 */
@Data
public class Processed {
    private String transaction_id;
}

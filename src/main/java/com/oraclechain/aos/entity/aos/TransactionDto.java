package com.oraclechain.aos.entity.aos;

import lombok.Data;

/**
 * @ClassName: TransactionDto
 * @Description: Todo
 * @data: 2020/8/3  18:48
 */
@Data
public class TransactionDto {
    private String id;
    private Long block_num;
    private Long last_irreversible_block;


}

package com.oraclechain.aos.entity;

import com.oraclechain.aos.common.model.SuperEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 用户账户
 * </p>
 *
 * @since 2020-07-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="UserAccount对象", description="用户账户")
public class UserAccount extends SuperEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "AOS余额")
    private Long aosBalance;

    @ApiModelProperty(value = "USDT余额")
    private Long usdtBalance;

}

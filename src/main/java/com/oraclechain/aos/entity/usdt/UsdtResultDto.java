package com.oraclechain.aos.entity.usdt;

import lombok.Data;

/**
 * @ClassName: UsdtResultDto @Description: Todo
 *
 * @data: 2020/8/5 12:36
 */
@Data
public class UsdtResultDto {
  private String status;
  private String message;
  private String result;
}

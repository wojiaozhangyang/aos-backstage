package com.oraclechain.aos.entity.usdt;

/**
 * @ClassName: GasResultDto @Description: Todo
 *
 * @data: 2020/8/5 11:19
 */
public class GasResultDto {
  private String status;
  private String message;
  private GasDto result;

  public void setStatus(String status) {
    this.status = status;
  }

  public String getStatus() {
    return status;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getMessage() {
    return message;
  }

  public void setResult(GasDto result) {
    this.result = result;
  }

  public GasDto getResult() {
    return result;
  }
}

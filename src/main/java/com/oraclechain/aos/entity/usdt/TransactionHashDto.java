package com.oraclechain.aos.entity.usdt;

import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName: TransactionHashDto
 * @Description: Todo
 * @data: 2020/8/6  17:35
 */
@Data
public class TransactionHashDto implements Serializable {
    private String jsonrpc;
    private Integer id;
    private String result;
}

package com.oraclechain.aos.entity.usdt;

/**
 * @ClassName: EthGetTransactionByHashDto
 * @Description: Todo
 * @data: 2020/8/6  15:33
 */
public class EthGetTransactionByHashDto {

        private String jsonrpc;
        private int id;
        private EthGetTransactionByHashParam result;

        public void setJsonrpc(String jsonrpc) {
            this.jsonrpc = jsonrpc;
        }
        public String getJsonrpc() {
            return jsonrpc;
        }

        public void setId(int id) {
            this.id = id;
        }
        public int getId() {
            return id;
        }

        public void setResult(EthGetTransactionByHashParam result) {
            this.result = result;
        }
        public EthGetTransactionByHashParam getResult() {
            return result;
        }

    }
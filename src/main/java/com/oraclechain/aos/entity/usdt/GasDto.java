package com.oraclechain.aos.entity.usdt;

/**
 * @ClassName: GasDto
 * @Description: Todo
 * @data: 2020/8/5  11:19
 */
public class GasDto {
    private String LastBlock;
    private String SafeGasPrice;
    private String ProposeGasPrice;
    public void setLastBlock(String LastBlock) {
        this.LastBlock = LastBlock;
    }
    public String getLastBlock() {
        return LastBlock;
    }

    public void setSafeGasPrice(String SafeGasPrice) {
        this.SafeGasPrice = SafeGasPrice;
    }
    public String getSafeGasPrice() {
        return SafeGasPrice;
    }

    public void setProposeGasPrice(String ProposeGasPrice) {
        this.ProposeGasPrice = ProposeGasPrice;
    }
    public String getProposeGasPrice() {
        return ProposeGasPrice;
    }

}
package com.oraclechain.aos.entity.usdt;

import lombok.Data;

/**
 * @ClassName: Transaction
 * @Description: Todo
 * @data: 2020/8/6  11:11
 */
@Data
public class Transaction {
    private String apikey;
    private String hex;
    private String action;
    private String module;

}

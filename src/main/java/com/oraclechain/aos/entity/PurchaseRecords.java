package com.oraclechain.aos.entity;

import com.oraclechain.aos.common.model.SuperEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 申购记录
 * </p>
 *
 * @since 2020-07-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="PurchaseRecords对象", description="申购记录")
public class PurchaseRecords extends SuperEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "申购花费的usdt金额")
    private Long usdtAmountCosts;

    @ApiModelProperty(value = "aos实际更新的天数")
    private Integer aosActualDay;

    @ApiModelProperty(value = "aos实际奖励的次数")
    private Integer aosActualCount;

    @ApiModelProperty(value = "aos额外奖励次数")
    private Integer extraPredictCount;

    @ApiModelProperty(value = "aos额外天数")
    private Integer extraPredictDay;

    private Integer isRebate;

    private Integer isAmount;

}

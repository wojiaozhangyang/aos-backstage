package com.oraclechain.aos.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.oraclechain.aos.common.model.SuperEntity;
import java.time.LocalDateTime;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @since 2020-07-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="UsdtEthRecord对象", description="")
public class UsdtEthRecord extends SuperEntity {

    private static final long serialVersionUID = 1L;

    private String blockNumber;

    private String timeStamp;

    private String hash;

    private String nonce;

    private String blockHash;

    @TableField("`from`")
    private String from;

    private String contractAddress;
    @TableField("`to`")
    private String to;

    private String value;

    private String tokenName;

    private String tokenSymbol;

    private String tokenDecimal;

    private String transactionIndex;
    @TableField("`gas`")
    private String gas;

    private String gasPrice;

    private String gasUsed;

    private String cumulativeGasUsed;

    private String input;

    private String confirmations;

    private Integer status;

    private Integer isAmountRecord;


}

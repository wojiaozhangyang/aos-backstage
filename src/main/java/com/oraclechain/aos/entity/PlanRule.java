package com.oraclechain.aos.entity;

import com.oraclechain.aos.common.model.SuperEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @since 2020-07-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="PlanRule对象", description="")
public class PlanRule extends SuperEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "释放百分比例")
    private Integer releasePercentage;

    @ApiModelProperty(value = "释放次数")
    private Integer releaseCount;

    @ApiModelProperty(value = "额外奖励次数")
    private Integer extraPredictCount;


}

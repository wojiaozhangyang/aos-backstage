package com.oraclechain.aos.entity;

import com.oraclechain.aos.common.model.SuperEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 返佣记录

 * </p>
 *
 * @since 2020-07-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="RebateRecord对象", description="返佣记录 ")
public class RebateRecord extends SuperEntity {

    private static final long serialVersionUID = 1L;
    private String userEmail;
    @ApiModelProperty(value = "来自用户")
    private String fromUserId;

    @ApiModelProperty(value = "去用户")
    private String toUserId;

    @ApiModelProperty(value = "金额")
    private Long amount;

    @ApiModelProperty(value = "状态（0未返还成功，1返佣成功）")
    private Integer status;

    private Integer type;

    private Integer currentDay;
    private Integer totalDay;

    @ApiModelProperty(value = "申购花费的usdt金额")
    private Long usdtAmountCosts;
    @ApiModelProperty(value = "待释放的(1需要释放，0不释放，2审核成功，3释放成功")
    private Integer isRelease;
}

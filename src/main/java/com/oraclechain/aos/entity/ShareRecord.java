package com.oraclechain.aos.entity;

import com.oraclechain.aos.common.model.SuperEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 分享记录
 * </p>
 *
 * @since 2020-07-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="ShareRecord对象", description="分享记录")
public class ShareRecord extends SuperEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "链接")
    private String url;

    @ApiModelProperty(value = "屏幕截图")
    private String screenshot;

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "审核状态(0:未审核，1已通过)")
    private Integer status;

    @ApiModelProperty(value = "用户邮箱")
    private String userEmail;

}

package com.oraclechain.aos.entity;

import com.oraclechain.aos.common.model.SuperEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @since 2020-07-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="OutAmount对象", description="")
public class OutAmount extends SuperEntity {

    private static final long serialVersionUID = 1L;

    private String toAddress;

    private String fromAddrrss;

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "手续费")
    private Long procedureFee;

    @ApiModelProperty(value = "提币金额")
    private Long amount;

    @ApiModelProperty(value = "类型(1,aos提现。2usdt提币)")
    private Integer type;

    @ApiModelProperty(value = "状态（1成功。0未成功）")
    private Integer status;
    @ApiModelProperty(value = "审核（1成功。0未成功）")
    private Integer audit;


    @ApiModelProperty(value = "转账哈希")
    private String  transactionHash;
    @ApiModelProperty(value = "用户邮箱")
    private String userEmail;

}

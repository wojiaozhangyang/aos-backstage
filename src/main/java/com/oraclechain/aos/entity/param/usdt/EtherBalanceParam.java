package com.oraclechain.aos.entity.param.usdt;

import lombok.Data;


/**
 * @ClassName: EtherBalanceParam
 * @Description: Todo
 * @data: 2020/7/19  19:02
 */
@Data
public class EtherBalanceParam {
        private String status;
        private String message;
        private String result;


}

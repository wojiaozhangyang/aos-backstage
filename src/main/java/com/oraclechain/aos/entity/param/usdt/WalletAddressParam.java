package com.oraclechain.aos.entity.param.usdt;

import lombok.Data;

/**
 * @ClassName: WalletAddressParam
 * @Description: Todo
 * @data: 2020/7/21  17:55
 */
@Data
public class WalletAddressParam {
    private String walletAddress;
    private String privateKey;
}

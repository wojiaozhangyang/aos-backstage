package com.oraclechain.aos.entity.param.usdt;

import lombok.Data;

/**
 * @ClassName: TxReceiptStatusVo
 * @Description: Todo
 * @data: 2020/7/21  11:05
 */
@Data
public class TxReceiptStatusVo {
    private String status;

    private String message;

    private TxReceiptStatusParam result;


}

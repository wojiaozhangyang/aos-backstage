package com.oraclechain.aos.entity.param.Record;

import lombok.Data;

import java.util.Date;

/**
 * @ClassName: MyUsdtRecord
 * @Description: Todo
 * @data: 2020/7/28  21:11
 */
@Data
public class MyUsdtRecord {

    private Long amount;
    private Date createTime;
    private String  type;
    private Integer symbol;

}

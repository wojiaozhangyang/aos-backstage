package com.oraclechain.aos.entity.param.Record;

import lombok.Data;

import java.util.ArrayList;

/**
 * @ClassName: UsdtToAosRecordParam
 * @Description: Todo
 * @data: 2020/7/24  11:11
 */
@Data
public class UsdtToAosRecordParam {

    private String code;
    private String msg;
    private ArrayList data;
    private String message;


}

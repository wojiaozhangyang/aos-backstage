package com.oraclechain.aos.entity.param.usdt;

import com.oraclechain.aos.entity.UsdtEthRecord;
import lombok.Data;

import java.util.List;

/**
 * @ClassName: UsdtEthRecordPage
 * @Description: Todo
 * @data: 2020/7/19  16:31
 */
@Data
public class UsdtEthRecordPage {

    private String status;
    private String message;
    private List<UsdtEthRecord> result;
}

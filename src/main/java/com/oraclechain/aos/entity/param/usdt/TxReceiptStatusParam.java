package com.oraclechain.aos.entity.param.usdt;

import lombok.Data;

/**
 * @ClassName: TxReceiptStatusParam
 * @Description: Todo
 * @data: 2020/7/21  11:02
 */
@Data
public class TxReceiptStatusParam {
    private String status;
}



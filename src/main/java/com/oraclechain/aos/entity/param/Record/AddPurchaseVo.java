package com.oraclechain.aos.entity.param.Record;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;


/**
 * @ClassName: addVo
 * @Description: Todo
 * @data: 2020/7/16  18:20
 */
@Data
@ApiModel(value="PurchaseRecords对象", description="申购记录")
public class AddPurchaseVo  {

    private static final long serialVersionUID = 1L;



    @NotNull(message = "申购金额不能为空")
    @ApiModelProperty(value = "申购花费的usdt金额")
    private Long usdtAmountCosts;






}


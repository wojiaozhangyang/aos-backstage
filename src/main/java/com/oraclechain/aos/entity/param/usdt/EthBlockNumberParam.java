package com.oraclechain.aos.entity.param.usdt;

import lombok.Data;

/**
 * @ClassName: ethBlockNumberParam
 * @Description: Todo
 * @data: 2020/7/19  19:43
 */
@Data
public class EthBlockNumberParam {

    private String jsonrpc;
    private Long id;
    private String result;
}

package com.oraclechain.aos.entity.export;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;


/**
 * <p>
 * 用户表
 * </p>
 *
 * @since 2020-07-14
 */
@Data

public class UserExport  implements Serializable {

    private static final long serialVersionUID = 1L;

    @Excel(name = "邀请人")
    private String email;

    @Excel(name = "被邀请人")
    private String inviterBy;

    @Excel(name = "直推/间推")
    @ApiModelProperty(value = "用户账号")
    private String pushInfo;


}

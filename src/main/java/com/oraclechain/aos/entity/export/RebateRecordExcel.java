package com.oraclechain.aos.entity.export;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName: RebateRecordExcel
 * @Description: Todo
 * @data: 2020/7/28  14:38
 */
@Data
public class RebateRecordExcel  implements Serializable {
    private static final long serialVersionUID = 1L;
    @Excel(name = "用户邮箱",width = 30)
    private String userEmail;
    @Excel(name = "来自用户",width = 30)
    private String fromUserId;
    @Excel(name = "去用户",width = 30)
    private String toUserId;
    @Excel(name = "金额",width = 30)
    private Long amount;

    @Excel(name = "状态",replace = {"返佣成功_1","未返还成功_0"})
    private String status;
    @Excel(name = "类型",replace = {"AOS返佣_1","USDT返佣_2"})
    private String type;
    @Excel(name = "当前返佣天",width = 30)
    private Integer currentDay;
    @Excel(name = "总计返佣天",width = 30)
    private Integer totalDay;
}

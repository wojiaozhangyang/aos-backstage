package com.oraclechain.aos.entity.export;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName: PurchaseExcel
 * @Description: Todo
 * @data: 2020/7/28  11:57
 */
@Data
public class PurchaseExcel  implements Serializable {
    private static final long serialVersionUID = 1L;
    @Excel(name = "用户id",width = 30)
    private String userId;

    @Excel(name = "申购花费的usdt金额",width = 30)
    private Long usdtAmountCosts;

    @Excel(name = "AOS应发放天数",width = 30)
    private Integer aosActualDay;

    @Excel(name = "AOS已发放次数",width = 30)
    private Integer aosActualCount;

    @Excel(name = "AOS额外奖励发放次数",width = 30)
    private Integer extraPredictCount;

    @Excel(name = "AOS额外奖励共计天数",width = 30)
    private Integer extraPredictDay;

}

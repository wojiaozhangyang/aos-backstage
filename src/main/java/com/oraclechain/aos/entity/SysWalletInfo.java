package com.oraclechain.aos.entity;

import com.oraclechain.aos.common.model.SuperEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @since 2020-08-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="SysWalletInfo对象", description="")
public class SysWalletInfo extends SuperEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "usdt平台钱包地址")
    private String sysWalletUsdtAddress;

    @ApiModelProperty(value = "usdt平台钱包私钥")
    @TableField("sys_wallet_usdt_privateKey")
    private String sysWalletUsdtPrivatekey;

    @ApiModelProperty(value = "aos平台钱包地址")
    private String sysWallAosAddress;


}

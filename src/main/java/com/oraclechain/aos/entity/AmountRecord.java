package com.oraclechain.aos.entity;

import com.oraclechain.aos.common.model.SuperEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @since 2020-07-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="AmountRecord对象", description="")
public class AmountRecord extends SuperEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "类型：(1:aos充值,2usdt充值，5aos提币，6usdt提币)")
    private Integer type;


    @ApiModelProperty(value = "aos金额")
    private Long aosAmount;

    @ApiModelProperty(value = "钱包地址")
    private String walletAddress;

    @ApiModelProperty(value = "usdt金额")
    private String usdtAmount;

    @ApiModelProperty(value = "状态")
    private Integer status;

    private String usdtEthRecordId;
}

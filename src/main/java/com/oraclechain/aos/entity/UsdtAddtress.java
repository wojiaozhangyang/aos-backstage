package com.oraclechain.aos.entity;

import com.oraclechain.aos.common.model.SuperEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @since 2020-08-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="UsdtAddtress对象", description="")
public class UsdtAddtress extends SuperEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "usdt地址")
    private String address;

    @ApiModelProperty(value = "0未使用。1已经使用")
    private Integer isUse;


}
